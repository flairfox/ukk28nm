/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_StudentActivityMonitoring;

import atom.network.ATOMListeningSocket_Abstract;
import atom.network.ATOMNetworkCommand;
import atom.network.ATOMNetworkProtocol;
import atom.network.ATOMSocket_Abstract;
import java.util.HashMap;
import java.util.UUID;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogicOwner;
import atom.events2.IATOMEvent;
import atom.names.ATOMName;
import atom.names.IATOMName;
import ukk28nm.Protocols.EUKK28NM_ProtocolSideRoles;
import ukk28nm.Protocols.Protocol_GroupLessonOrganization.Protocol_GroupLessonOrganization;
import ukk28nm.Protocols.Protocol_UsersRegistration.Protocol_UserRegistration;

/**
 *
 * @author Vadim
 */
public class Protocol_StudentActivityMonitoring extends ATOMNetworkProtocol implements IATOMEventDelegateOwner, IATOMEventLogicOwner
{

    protected static UUID PROTOCOL_ID = UUID.fromString("c21def5d-4cf7-42c2-b2bd-57971b08a115");

    /**
     * Сокет, используемый протоколом в роли ServerSide для ожидания новых
     * внешних подключений
     */
    protected ATOMListeningSocket_Abstract _ListeningSocket;

    /**
     * Сокет, используемый протоколом в роли ClientSide для передачи сообщений
     * на сервер
     */
    protected ATOMSocket_Abstract _WorkSocket;

    protected Protocol_UserRegistration _UserRegistration;
    protected Protocol_GroupLessonOrganization _GroupLessonOrganization;

    protected HashMap<UUID, UUID> _MonitoringMap;
    protected boolean _isMonitoringModeActive = false;

    public Protocol_StudentActivityMonitoring(ProtocolSideRole protocolSide, Protocol_UserRegistration userRegistration, Protocol_GroupLessonOrganization groupLessonOrganization, ATOMListeningSocket_Abstract listeningSocket)
    {
        super(protocolSide);
        AddListeningSocket(listeningSocket);

        _UserRegistration = userRegistration;
        _GroupLessonOrganization = groupLessonOrganization;

        _MonitoringMap = new HashMap<UUID, UUID>();

        _Delegate = new EventLogic();
    }

    public Protocol_StudentActivityMonitoring(ProtocolSideRole protocolSide, Protocol_UserRegistration userRegistration, Protocol_GroupLessonOrganization groupLessonOrganization, ATOMSocket_Abstract workSocket)
    {
        super(protocolSide);
        AddWorkSocket(workSocket);

        _WorkSocket = workSocket;

        _UserRegistration = userRegistration;
        _GroupLessonOrganization = groupLessonOrganization;

        _Delegate = new EventLogic();
    }

    public void StartMonitoring(UUID studentID)
    {
        Command_StartMonitoring startMonitoringCommand = new Command_StartMonitoring(PROTOCOL_ID);
        startMonitoringCommand.StudentID = studentID;

        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.TEACHER))
        {
            SendCommand(startMonitoringCommand, _WorkSocket);
        }

        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            SendCommand(startMonitoringCommand, _UserRegistration.FindUserCommunicationSocket(studentID));
        }
    }

    public void StopMonitoring()
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.TEACHER))
        {
            Command_StopMonitoring stopMonitoringCommand = new Command_StopMonitoring(PROTOCOL_ID);
            SendCommand(stopMonitoringCommand, _WorkSocket);
        }
    }

    protected void StopMonitoring(UUID studentID)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_StopMonitoring stopMonitoringCommand = new Command_StopMonitoring(PROTOCOL_ID);
            SendCommand(stopMonitoringCommand, _UserRegistration.FindUserCommunicationSocket(studentID));
        }
    }

    public void UpdateSection(UUID sectionID)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
        {
            Command_UpdateSection updateSectionCommand = new Command_UpdateSection(PROTOCOL_ID);
            updateSectionCommand.SectionID = sectionID;

            SendCommand(updateSectionCommand, _WorkSocket);
        }
    }

    protected void UpdateSection(UUID sectionID, UUID studentID)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_UpdateSection updateSectionCommand = new Command_UpdateSection(PROTOCOL_ID);
            updateSectionCommand.SectionID = sectionID;

            UUID teacherID = null;
            for (UUID teacher : _MonitoringMap.keySet())
            {
                if (_MonitoringMap.get(teacher).equals(studentID))
                {
                    teacherID = teacher;
                }
            }

            SendCommand(updateSectionCommand, _UserRegistration.FindUserCommunicationSocket(teacherID));
        }
    }

    @Override
    public void Start()
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected boolean OnDo_ProcessIncomingCommand(ATOMNetworkProtocol.Events.IncomingNetworkCommandProcess event)
    {
        ATOMNetworkCommand command = event.IncomingCommand;

        if (command instanceof Command_StartMonitoring)
        { // Команда "Пользователь отправил запрос на открытие урока"
            Command_StartMonitoring incomingCommand = (Command_StartMonitoring) command;

            if (!_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.StartMonitoring.Rise(this);
            }

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.StartMonitoringRequest.Rise(this, _UserRegistration.FindUserID(event.SocketToResponceSender), incomingCommand.StudentID);
            }
        }

        if (command instanceof Command_UpdateSection)
        { // Команда "Обновление раздела"
            Command_UpdateSection incomingCommand = (Command_UpdateSection) command;

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.TEACHER))
            {
                Events.UpdateSection.Rise(this, incomingCommand.SectionID);
            }

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.UpdateSectionRequest.Rise(this, incomingCommand.SectionID, _UserRegistration.FindUserID(event.SocketToResponceSender));
            }
        }

        if (command instanceof Command_StopMonitoring)
        { // Команда "Пользователь отправил запрос на открытие урока"
            Command_StopMonitoring incomingCommand = (Command_StopMonitoring) command;

            if (!_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.StopMonitoring.Rise(this);
            }

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.StopMonitoringRequest.Rise(this, _UserRegistration.FindUserID(event.SocketToResponceSender));
            }
        }

        return true;
    }

    private boolean OnDo_StartMonitoringRequest(Events.StartMonitoringRequest event)
    {
        _MonitoringMap.put(event.TeacherID, event.StudentID);
        StartMonitoring(event.StudentID);

        return true;
    }

    private boolean OnDo_StartMonitoring(Events.StartMonitoring event)
    {
        _isMonitoringModeActive = true;

        return true;
    }

    private boolean OnDo_UpdateSectionRequest(Events.UpdateSectionRequest event)
    {
        UpdateSection(event.SectionID, event.StudentID);

        return true;
    }

    private boolean OnDo_UpdateSection(Events.UpdateSection event)
    {
        UpdateSection(event.SectionID);

        return true;
    }

    private boolean OnDo_StopMonitoringRequest(Events.StopMonitoringRequest event)
    {
        if (_MonitoringMap.containsKey(event.TeacherID))
        {
            StopMonitoring(_MonitoringMap.get(event.TeacherID));
            _MonitoringMap.remove(event.TeacherID);
        }

        return true;
    }

    private boolean OnDo_StopMonitoring(Events.StopMonitoring event)
    {
        _isMonitoringModeActive = false;

        return true;
    }

    @Override
    protected ATOMNetworkCommand CreateCommandByName(String commandName)
    {
        if (Command_StartMonitoring.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_StartMonitoring(PROTOCOL_ID);
        }

        if (Command_UpdateSection.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_UpdateSection(PROTOCOL_ID);
        }

        if (Command_StopMonitoring.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_StopMonitoring(PROTOCOL_ID);
        }

        return null;
    }

    public boolean isMonitoringModeActive()
    {
        return _isMonitoringModeActive;
    }

    @Override
    public UUID getProtocolID()
    {
        return PROTOCOL_ID;
    }

    //========================================================
    // --- ТИПЫ СОБЫТИЙ класса EventSource
    public abstract static class Events extends ATOMNetworkProtocol.Events
    {

        protected Events(IATOMEventDelegateOwner eventSource)
        {
            super((ATOMNetworkProtocol) eventSource);
        }

        protected Events(IATOMEventDelegateOwner eventSource, Object eventTarget)
        {
            super(eventSource, eventTarget);
        }

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 1
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Запрос на начало отслеживания работы студента" #####">
        /**
         * Класс экземпляра события "Запрос на начало отслеживания работы
         * студента"
         */
        protected static class StartMonitoringRequest extends Events
        {

            /**
             * Тип события "Запрос на начало отслеживания работы студента"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.StartMonitoringRequest");

            //Параметры события
            UUID TeacherID;
            UUID StudentID;

            public StartMonitoringRequest(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StartMonitoringRequest(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StartMonitoringRequest Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID teacherID, UUID studentID)
            {
                StartMonitoringRequest e = new StartMonitoringRequest(eventSource, eventTarget);
                e.TeacherID = teacherID;
                e.StudentID = studentID;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Запрос на начало
             * отслеживания работы студента* @param eventSource Источник
             * события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_StudentActivityMonitoring eventSource, UUID teacherID, UUID studentID)
            {
                StartMonitoringRequest e = Create(eventSource, eventSource, teacherID, studentID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Запрос на начало отслеживания работы студента""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Начало мониторинга" #####">
        /**
         * Класс экземпляра события "Начало мониторинга"
         */
        public static class StartMonitoring extends Events
        {

            /**
             * Тип события "Начало мониторинга"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.StartMonitoring");

            //Параметры события
            public StartMonitoring(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StartMonitoring(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StartMonitoring Create(IATOMEventDelegateOwner eventSource, Object eventTarget /*, int parametr1, int parametr2*/)
            {
                StartMonitoring e = new StartMonitoring(eventSource, eventTarget);
                //e.parametr1 = parametr1;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Начало мониторинга
             *
             *
             * @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_StudentActivityMonitoring eventSource /*, int parametr1, int parametr2*/)
            {
                StartMonitoring e = Create(eventSource, eventSource/*,param1, param2*/);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Начало мониторинга""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Запрос на обновление раздела" #####">
        /**
         * Класс экземпляра события "Запрос на обновление раздела"
         */
        protected static class UpdateSectionRequest extends Events
        {

            /**
             * Тип события "Запрос на обновление раздела"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.MyEvent1");

            //Параметры события
            UUID SectionID;
            UUID StudentID;

            public UpdateSectionRequest(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public UpdateSectionRequest(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static UpdateSectionRequest Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID sectionID, UUID studentID)
            {
                UpdateSectionRequest e = new UpdateSectionRequest(eventSource, eventTarget);
                e.SectionID = sectionID;
                e.StudentID = studentID;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Запрос на обновление
             * раздела* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_StudentActivityMonitoring eventSource, UUID sectionID, UUID studentID)
            {
                UpdateSectionRequest e = Create(eventSource, eventSource, sectionID, studentID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Запрос на обновление раздела""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Обновление раздела" #####">
        /**
         * Класс экземпляра события "Обновление раздела"
         */
        public static class UpdateSection extends Events
        {

            /**
             * Тип события "Обновление раздела"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.UpdateSection");

            //Параметры события
            public UUID SectionID;

            //int parametr1;
            //int parametr2;
            public UpdateSection(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public UpdateSection(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static UpdateSection Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID sectionID)
            {
                UpdateSection e = new UpdateSection(eventSource, eventTarget);
                e.SectionID = sectionID;
                //e.parametr1 = parametr1;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Обновление раздела
             *
             *
             * @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_StudentActivityMonitoring eventSource, UUID sectionID)
            {
                UpdateSection e = Create(eventSource, eventSource, sectionID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Обновление раздела""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Запрос на остановку мониторинга студента" #####">
        /**
         * Класс экземпляра события "Запрос на остановку мониторинга студента"
         */
        protected static class StopMonitoringRequest extends Events
        {

            /**
             * Тип события "Запрос на остановку мониторинга студента"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.StopMonitoringRequest");

            //Параметры события
            UUID TeacherID;

            //int parametr1;
            //int parametr2;
            public StopMonitoringRequest(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StopMonitoringRequest(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StopMonitoringRequest Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID teacherID)
            {
                StopMonitoringRequest e = new StopMonitoringRequest(eventSource, eventTarget);
                e.TeacherID = teacherID;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Запрос на остановку
             * мониторинга студента* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_StudentActivityMonitoring eventSource, UUID teacherID)
            {
                StopMonitoringRequest e = Create(eventSource, eventSource, teacherID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Запрос на остановку мониторинга студента""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Остановка мониторинга студента" #####">
        /**
         * Класс экземпляра события "Остановка мониторинга студента"
         */
        public static class StopMonitoring extends Events
        {

            /**
             * Тип события "Остановка мониторинга студента"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.StopMonitoring");

            //Параметры события
            //int parametr1;
            //int parametr2;
            public StopMonitoring(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StopMonitoring(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StopMonitoring Create(IATOMEventDelegateOwner eventSource, Object eventTarget /*, int parametr1, int parametr2*/)
            {
                StopMonitoring e = new StopMonitoring(eventSource, eventTarget);
                //e.parametr1 = parametr1;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Остановка мониторинга
             * студента* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_StudentActivityMonitoring eventSource /*, int parametr1, int parametr2*/)
            {
                StopMonitoring e = Create(eventSource, eventSource/*,param1, param2*/);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Остановка мониторинга студента""
//##################################################################

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 2
    }; // ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################

    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ATOMNetworkProtocol.EventLogic        //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //    isAllowEvent = OnBefore_Event1(event);
            // }
            //2. Событие Events.Event2
            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (Events.StartMonitoringRequest.class.isAssignableFrom(eventClass))
            {
                Events.StartMonitoringRequest event = (Events.StartMonitoringRequest) e;
                isDone = OnDo_StartMonitoringRequest(event);
            }

            if (Events.StartMonitoring.class.isAssignableFrom(eventClass))
            {
                Events.StartMonitoring event = (Events.StartMonitoring) e;
                isDone = OnDo_StartMonitoring(event);
            }

            if (Events.UpdateSectionRequest.class.isAssignableFrom(eventClass))
            {
                Events.UpdateSectionRequest event = (Events.UpdateSectionRequest) e;
                isDone = OnDo_UpdateSectionRequest(event);
            }

            if (Events.UpdateSection.class.isAssignableFrom(eventClass))
            {
                Events.UpdateSection event = (Events.UpdateSection) e;
                isDone = OnDo_UpdateSection(event);
            }

            if (Events.StopMonitoringRequest.class.isAssignableFrom(eventClass))
            {
                Events.StopMonitoringRequest event = (Events.StopMonitoringRequest) e;
                isDone = OnDo_StopMonitoringRequest(event);
            }

            if (Events.StopMonitoring.class.isAssignableFrom(eventClass))
            {
                Events.StopMonitoring event = (Events.StopMonitoring) e;
                isDone = OnDo_StopMonitoring(event);
            }

            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //
            // }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
    } //--- END CLASS EventLogic

//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################
}
