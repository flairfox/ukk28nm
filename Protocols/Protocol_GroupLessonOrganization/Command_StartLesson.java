/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_GroupLessonOrganization;

import atom.errors.ATOMError;
import atom.names.ATOMStringResource;
import atom.names.IATOMName;
import atom.network.ATOMNetworkCommand;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Vadim
 */
public class Command_StartLesson extends ATOMNetworkCommand
{

    public static IATOMName COMMAND_NAME = new ATOMStringResource("Command_StartLesson", "Запрос на создание урока");
    public static IATOMName TEACHER_ID = new ATOMStringResource("TeacherID", "ID преподавателя, открывшего урок");
    public static IATOMName LESSON_ID = new ATOMStringResource("LessonID", "ID урока");

    public UUID TeacherID;
    public UUID LessonID;

    public Command_StartLesson(UUID protocolID)
    {
        _CommonBlock.ProtocolID = protocolID;
    }
    
    @Override
    public IATOMName GetCommandName()
    {
        return COMMAND_NAME;
    }

    @Override
    public void Serialize(JSONObject rawNetworkPackage)
    {
        super.Serialize(rawNetworkPackage);

        try
        {
            // Сериализация полей команды и командых блоков (при наличии)
            rawNetworkPackage.put(TEACHER_ID.LocalName(), TeacherID.toString());

            rawNetworkPackage.put(LESSON_ID.LocalName(), LessonID.toString());

        } catch (JSONException ex)
        {
            Logger.getLogger(Command_StartLesson.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.Serialize(rawNetworkPackage); // записываем базовые данные команды
    }

    @Override
    public ATOMError Deserialize(JSONObject rawNetworkPackage)
    {
        super.Deserialize(rawNetworkPackage); // Расшифровываем базовые данные команды

        TeacherID = UUID.fromString(GetStringValue(TEACHER_ID.LocalName()));

        LessonID = UUID.fromString(GetStringValue(LESSON_ID.LocalName()));

        return null;
    }
}
