/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_GroupLessonOrganization;

import DataDomains.SchemaATOM.SchemaEducation.I_PeopleGroupOnPeriod;
import DataDomains.SchemaATOM.SchemaEducation.LearningGroup;
import DataDomains.SchemaATOM.SchemaEducation.LearningGroupsThread;
import DataDomains.SchemaATOM.SchemaEducation.LearningSubgroup;
import DataDomains.SchemaATOM.SchemaEducation.LessonMeetingInTimeTable;
import DataDomains.SchemaATOM.SchemaEducation.SchemaEducation;
import DataDomains.SchemaATOM.SchemaEducation.Teacher;
import DataDomains.SchemaATOM.SchemaEducation.Trainee;
import atom.data.datastorage.DataEntityStorage;
import atom.data.entities.DataContext;
import atom.events2.ATOMEventListenerAdapter;
import atom.network.ATOMNetworkCommand;
import atom.network.ATOMNetworkProtocol;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogicOwner;
import atom.events2.IATOMEvent;
import atom.names.ATOMName;
import atom.names.IATOMName;
import atom.network.ATOMListeningSocket_Abstract;
import atom.network.ATOMSocket_Abstract;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.UUID;
import ukk28nm.Protocols.EUKK28NM_ProtocolSideRoles;
import ukk28nm.Protocols.Protocol_UsersRegistration.Protocol_UserRegistration;

/**
 *
 * @author Vadim
 */
public class Protocol_GroupLessonOrganization extends ATOMNetworkProtocol implements IATOMEventDelegateOwner, IATOMEventLogicOwner
{

    protected static UUID PROTOCOL_ID = UUID.fromString("894a3e90-97ca-4fad-ab05-ba99ce9dfa59");
    public static UUID DEFAULT_LESSON_GUID = UUID.fromString("db8ba65a-d151-4267-a99b-b9dc5e85c72c");

    protected HashMap<UUID, UKK28NM_Lesson> _Lessons;
    protected Protocol_UserRegistration _UserRegistrationProtocol;

    public ATOMSocket_Abstract _WorkSocket;

    public Protocol_GroupLessonOrganization(ProtocolSideRole protocolSide,
            Protocol_UserRegistration userRegistrationProtocol,
            ATOMListeningSocket_Abstract listeningSocket)
    {
        super(protocolSide);
        AddListeningSocket(listeningSocket);

        _Delegate = new EventLogic();

        _UserRegistrationProtocol = userRegistrationProtocol;
        _UserRegistrationProtocol.Delegate().AddListener(_UserRegistrationListener);

        if (protocolSide.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            _Lessons = new HashMap<UUID, UKK28NM_Lesson>();
        }
    }

    public Protocol_GroupLessonOrganization(ProtocolSideRole protocolSide, Protocol_UserRegistration userRegistrationProtocol, ATOMSocket_Abstract workSocket)
    {
        super(protocolSide);
        AddWorkSocket(workSocket);

        _WorkSocket = workSocket;

        _Delegate = new EventLogic();
        _UserRegistrationProtocol = userRegistrationProtocol;
        _UserRegistrationProtocol.Delegate().AddListener(_CommonListener);
    }

    public void StartLesson(Teacher teacher, LessonMeetingInTimeTable lesson)
    {
        // Создание урока
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.TEACHER))
        {
            /**
             * Протокол преподавателя создает и отправляет на сервер команду
             * сохания нового занятия
             */
            Command_StartLesson startLesson = new Command_StartLesson(PROTOCOL_ID);
            if (teacher == null)
                 startLesson.TeacherID = DataEntityStorage.NULL_GUID; // !!! ВРЕМЕННО !!!
            else
                 startLesson.TeacherID = teacher.aRefPerson().GUID();
            
            if (lesson != null)
            {
                startLesson.LessonID = lesson.GUID();
            } else
            {
                startLesson.LessonID = DEFAULT_LESSON_GUID; // !!! ВРЕМЕННО !!!
            }
            SendCommand(startLesson, _UserRegistrationProtocol._WorkSocket);
        }
    }

    public void StopLesson()
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.TEACHER))
        {
            Command_StopLesson stopLesson = new Command_StopLesson(PROTOCOL_ID);
            
            
            SendCommand(stopLesson, _WorkSocket);
        }
    }

    protected void StopLesson(UUID teacherID)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_StopLesson stopLesson = new Command_StopLesson(PROTOCOL_ID);
            UUID lessonToStopID = FindLessonIDByTeacherID(teacherID);
            if (lessonToStopID != null)
            {
                LinkedList<UUID> studentsIDs_Present = _Lessons.get(lessonToStopID).getStudentsIDs_Present();

                if (!studentsIDs_Present.isEmpty())
                {
                    for (UUID studentID : FindLessonByTeacherID(teacherID).getStudentsIDs_Present())
                    {
                        SendCommand(stopLesson, _UserRegistrationProtocol.FindUserCommunicationSocket(studentID));
                    }
                }

                _Lessons.remove(lessonToStopID);
            }
        }
    }

    protected void SendLessonCreationNotifications(UUID lessonID, UUID teacherID, LinkedList<UUID> notifyList)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_NotifyParticipants lessonCreationNotification = new Command_NotifyParticipants(PROTOCOL_ID);
            lessonCreationNotification.LessonID = lessonID;
            lessonCreationNotification.TeacherID = teacherID;
            
            if (!_UserRegistrationProtocol.getRegisteredStudents().isEmpty())
            {
                for (UUID studentID : notifyList)
                {
                    if(_UserRegistrationProtocol.getRegisteredStudents().contains(studentID))
                    {
                        SendCommand(lessonCreationNotification, _UserRegistrationProtocol.FindUserCommunicationSocket(studentID));
                    }
                }
            }
        }
    }

    public void JoinLesson(UUID studentID, UUID lessonID)
    {
        Command_JoinLesson joinLesson = new Command_JoinLesson(PROTOCOL_ID);
        joinLesson.StudentID = studentID;
        joinLesson.LessonID = lessonID;

        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
        {
            SendCommand(joinLesson, _UserRegistrationProtocol._WorkSocket);
        }
    }

    public void RejectNotification(UUID studentID, UUID lessonID)
    {
        Command_RejectNotification rejectNotification = new Command_RejectNotification(PROTOCOL_ID);
        rejectNotification.StudentID = studentID;
        rejectNotification.LessonID = lessonID;

        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
        {
            SendCommand(rejectNotification, _UserRegistrationProtocol._WorkSocket);
        }
    }

    public void LeaveLesson(UUID studentID)
    {
        Command_LeaveLesson leaveLesson = new Command_LeaveLesson(PROTOCOL_ID);
        leaveLesson.StudentID = studentID;

        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
        {
            SendCommand(leaveLesson, _UserRegistrationProtocol._WorkSocket);
        }
    }

    protected UUID findTeacherIDByLessonID(UUID lessonID)
    {
        return _Lessons.get(lessonID).getTeacherID();
    }

    private boolean OnDo_StartLessonRequest(Events.StartLessonRequest event)
    {
        LessonMeetingInTimeTable lesson = DataContext.FindOrLoadEntityByUUID(event.LessonID,
                SchemaEducation.LessonMeetingInTimeTableDesc);

        LinkedList<UUID> studentIDsToNotifyList = CreateStudentIDsToNotifyList(lesson);
        
        /**
         * Временная заглушка, создающая список студентов на занятие из всех поключенных студентов
         */
        if (studentIDsToNotifyList.isEmpty())
        {
            studentIDsToNotifyList.addAll(_UserRegistrationProtocol.getRegisteredStudents());
        }

        _Lessons.put(event.LessonID, new UKK28NM_Lesson(event.LessonID, event.TeacherID, studentIDsToNotifyList));

        SendLessonCreationNotifications(event.LessonID, event.TeacherID, studentIDsToNotifyList);

        return true;
    }

    protected LinkedList<UUID> CreateStudentIDsToNotifyList(LessonMeetingInTimeTable lesson)
    {
        LinkedList<UUID> studentIDsList = new LinkedList<UUID>();

        I_PeopleGroupOnPeriod peopleGroup = lesson.aRefI_PeopleGroupOnPeriod();

        if (peopleGroup instanceof LearningGroup)
        {
            FillStudentIDsListFromLearningGroup(studentIDsList, (LearningGroup) peopleGroup);
        } else if (peopleGroup instanceof LearningSubgroup)
        {
            FillStudentIDsListFromLearningSubgroup(studentIDsList, (LearningSubgroup) peopleGroup);
        } else if (peopleGroup instanceof LearningGroupsThread)
        {
            FillStudentIDsListFromLearningGroupsThread(studentIDsList, (LearningGroupsThread) peopleGroup);
        }

        return studentIDsList;
    }

    protected void FillStudentIDsListFromLearningGroup(LinkedList<UUID> studentIDsList, LearningGroup learningGroup)
    {
        for (Trainee trainee : learningGroup.aRefGroupMembersList())
        {
            studentIDsList.add(trainee.aRefPerson().GUID());
        }
    }

    protected void FillStudentIDsListFromLearningSubgroup(LinkedList<UUID> studentIDsList,
            LearningSubgroup learningSubgroup)
    {
        for (Trainee trainee : learningSubgroup.aRefPersonsInSubgroupList())
        {
            studentIDsList.add(trainee.aRefPerson().GUID());
        }
    }

    protected void FillStudentIDsListFromLearningGroupsThread(LinkedList<UUID> studentIDsList,
            LearningGroupsThread learningGroupsThread)
    {
        for (LearningGroup group : learningGroupsThread.aRefGroupsInLearningThreadList())
        {
            FillStudentIDsListFromLearningGroup(studentIDsList, group);
        }

        for (LearningSubgroup subgroup : learningGroupsThread.aRefSubgroupsInLearningThreadList())
        {
            FillStudentIDsListFromLearningSubgroup(studentIDsList, subgroup);
        }
    }

    public UUID FindTeacherIDByPresentStudentID(UUID studentID)
    {
        for (UUID lessonID : _Lessons.keySet())
        {
            if (_Lessons.get(lessonID).getStudentsIDs_Present().contains(studentID))
            {
                return _Lessons.get(lessonID).getTeacherID();
            }
        }

        return null;
    }

    private void OnAfter_UserConnected(Protocol_UserRegistration.Events.UserLoggedIn event)
    {
        for (UKK28NM_Lesson lesson : _Lessons.values())
        {
            UUID guidConnectedUser = event.UserID;
            LinkedList<UUID> StudentIDs_MustBePresent = lesson.getStudentsIDs_MustBePresent();
            boolean isStudentMustBePresent = StudentIDs_MustBePresent.contains(guidConnectedUser);
            if (isStudentMustBePresent)
            {
                Command_NotifyParticipants lessonCreationNotification = new Command_NotifyParticipants(PROTOCOL_ID);
                lessonCreationNotification.LessonID = lesson.getLessonID();
                lessonCreationNotification.TeacherID = lesson.getTeacherID();

                SendCommand(lessonCreationNotification, _UserRegistrationProtocol.FindUserCommunicationSocket(event.UserID));
            }
        }
    }

    private boolean OnDo_StudentJoinedLesson(Events.StudentJoinedLesson event)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {

            _Lessons.get(event.LessonID).setStudentAsPresent(event.StudentID);

            Command_JoinLesson joinLesson = new Command_JoinLesson(PROTOCOL_ID);
            joinLesson.StudentID = event.StudentID;
            joinLesson.LessonID = event.LessonID;

            SendCommand(joinLesson,
                    _UserRegistrationProtocol.FindUserCommunicationSocket(_Lessons.get(event.LessonID).getTeacherID()));
        }

        return true;
    }

    private boolean OnDo_StudentRejectedNotification(Events.StudentRejectedNotification event)
    {
        return true;
    }

    private boolean OnDo_StudentLeftLesson(Events.StudentLeftLesson event)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            UKK28NM_Lesson lesson = _Lessons.get(FindLessonIDByStudentID(event.StudentID));
            lesson.setStudentAsMustBePresent(event.StudentID);

            Command_LeaveLesson leaveLesson = new Command_LeaveLesson(PROTOCOL_ID);
            leaveLesson.StudentID = event.StudentID;

            SendCommand(leaveLesson, _UserRegistrationProtocol.FindUserCommunicationSocket(lesson.getTeacherID()));
        }

        return true;
    }

    private boolean OnDo_LessonStarted(Events.LessonStarted event)
    {
        return true;
    }

    private boolean OnDo_LessonStopped(Events.LessonStopped event)
    {
        return true;
    }

    @Override
    protected boolean OnDo_ProcessIncomingCommand(ATOMNetworkProtocol.Events.IncomingNetworkCommandProcess event)
    {
        ATOMNetworkCommand command = event.IncomingCommand;

        if (command instanceof Command_StartLesson)
        { // Команда "Пользователь отправил запрос на открытие урока"
            Command_StartLesson incomingCommand = (Command_StartLesson) command;
            Events.StartLessonRequest.Rise(this, incomingCommand.TeacherID, incomingCommand.LessonID);
        }

        if (command instanceof Command_StopLesson)
        { // Команда "Пользователь отправил запрос на открытие урока"
            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                StopLesson(_UserRegistrationProtocol.FindUserID(event.SocketToResponceSender));
                Events.TeacherStoppedLesson.Rise(this, _UserRegistrationProtocol.FindUserID(event.SocketToResponceSender));
            } else if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
            {
                Events.LessonStopped.Rise(this);
            }
        }

        if (command instanceof Command_NotifyParticipants)
        { // Команда "Оповещение о начале урока"
            Command_NotifyParticipants incomingCommand = (Command_NotifyParticipants) command;

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
            {
                Events.LessonStarted.Rise(this, incomingCommand.LessonID, incomingCommand.TeacherID);
            }
        }

        if (command instanceof Command_JoinLesson)
        { // Команда "Присодиниться к уроку"
            Command_JoinLesson incomingCommand = (Command_JoinLesson) command;
            Events.StudentJoinedLesson.Rise(this, incomingCommand.LessonID, incomingCommand.StudentID);
        }
        
        if (command instanceof Command_RejectNotification)
        { // Команда "Отклонить уведомление"
            Command_RejectNotification incomingCommand = (Command_RejectNotification) command;
            Events.StudentRejectedNotification.Rise(this, incomingCommand.LessonID, incomingCommand.StudentID);
        }

        if (command instanceof Command_LeaveLesson)
        { // Команда "Покинуть урок"
            Command_LeaveLesson incomingCommand = (Command_LeaveLesson) command;
            Events.StudentLeftLesson.Rise(this, incomingCommand.StudentID);
        }

        return true;
    }

    @Override
    public void Start()
    {
        
    }

    @Override
    protected ATOMNetworkCommand CreateCommandByName(String commandName)
    {
        if (Command_StartLesson.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_StartLesson(PROTOCOL_ID);
        }

        if (Command_StopLesson.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_StopLesson(PROTOCOL_ID);
        }

        if (Command_NotifyParticipants.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_NotifyParticipants(PROTOCOL_ID);
        }

        if (Command_JoinLesson.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_JoinLesson(PROTOCOL_ID);
        }

        if (Command_RejectNotification.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_RejectNotification(PROTOCOL_ID);
        }

        if (Command_LeaveLesson.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_LeaveLesson(PROTOCOL_ID);
        }

        return null;

    }

    public UKK28NM_Lesson FindLessonByTeacherID(UUID teacherID)
    {
        for (UUID lessonID : _Lessons.keySet())
        {
            if (_Lessons.get(lessonID).getTeacherID().equals(teacherID))
            {
                return _Lessons.get(lessonID);
            }
        }

        return null;
    }

    public UUID FindLessonIDByTeacherID(UUID teacherID)
    {
        for (UUID lessonID : _Lessons.keySet())
        {
            if (_Lessons.get(lessonID).getTeacherID().equals(teacherID))
            {
                return lessonID;
            }
        }

        return null;
    }
    
    protected UKK28NM_Lesson FindLessonByStudentID(UUID studentID)
    {
        for(UKK28NM_Lesson lesson : _Lessons.values())
        {
            if(lesson.getStudentsIDs_Present().contains(studentID))
            {
                return lesson;
            }
        }
        
        return null;
    }
    
    protected UUID FindLessonIDByStudentID(UUID studentID)
    {
        for (UUID lessonID : _Lessons.keySet())
        {
            if (_Lessons.get(lessonID).getStudentsIDs_Present().contains(studentID))
            {
                return lessonID;
            }
        }

        return null;
    }
    
    public LinkedList<UUID> GetStudentIDs_Present(UUID teacherID)
    {
        return FindLessonByTeacherID(teacherID).getStudentsIDs_Present();
    }

    @Override
    public UUID getProtocolID()
    {
        return PROTOCOL_ID;
    }

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="##### УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ #####">
    protected CommonListener _UserRegistrationListener = new CommonListener();

    protected class CommonListener extends ATOMEventListenerAdapter
    {

        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            //1. Обработка события TEventSource.Events.Event1
            //if (TEventSource.Events.Event1.class.isAssignableFrom(eventClass))
            //{
            //  TEventSource.Events.Event1 event = (TEventSource.Events.Event1) e;
            //  isAllowEvent = OnBefore_Event1(event);
            //}
            return isAllowEvent;
        }

        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработка события TEventSource.Events.Event1
//            if (Protocol_UserRegistration.Events.UserConnected.class.isAssignableFrom(eventClass))
//            {
//                Protocol_UserRegistration.Events.UserConnected event = (Protocol_UserRegistration.Events.UserConnected) e;
//                isDone = OnDo_UserConnected(event);
//            }
            return isDone;
        }

        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            //1. Обработка события TEventSource.Events.Event1
            Class<?> eventClass = e.getClass();

            if (Protocol_UserRegistration.Events.UserLoggedIn.class.isAssignableFrom(eventClass))
            {
                Protocol_UserRegistration.Events.UserLoggedIn event = (Protocol_UserRegistration.Events.UserLoggedIn) e;
                OnAfter_UserConnected(event);
            }
        }
    }
//</editor-fold> КОНЕЦ "УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ"
//##################################################################

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="ТИПЫ СОБЫТИЙ класса EventSource">
    //========================================================
    // --- ТИПЫ СОБЫТИЙ класса EventSource
    public abstract static class Events extends ATOMNetworkProtocol.Events
    {

        protected Events(IATOMEventDelegateOwner eventSource)
        {
            super((ATOMNetworkProtocol) eventSource);
        }

        protected Events(IATOMEventDelegateOwner eventSource, Object eventTarget)
        {
            super(eventSource, eventTarget);
        }

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 1
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Отправка запроса на открытие урока" #####">
        /**
         * Класс экземпляра события "Запрос на открытие урока"
         */
        protected static class StartLessonRequest extends Events
        {

            /**
             * Тип события "Отправка запроса на открытие урока"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.LessonCreationRequest");

            //Параметры события
            public UUID TeacherID;
            public UUID LessonID;

            //int parametr2;
            public StartLessonRequest(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StartLessonRequest(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StartLessonRequest Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID userID, UUID lessonID)
            {
                StartLessonRequest e = new StartLessonRequest(eventSource, eventTarget);
                e.TeacherID = userID;
                e.LessonID = lessonID;

                return e;
            }

            /**
             * Генерация экземпляра события
             * "StudentRejectedNotification".Отправка запроса на открытие урока*
             * @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_GroupLessonOrganization eventSource, UUID userID, UUID lessonID)
            {
                StartLessonRequest e = Create(eventSource, eventSource, userID, lessonID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Отправка запроса на открытие урока""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Отправка запроса на закрытие урока" #####">
        /**
         * Класс экземпляра события "Запрос на закрытие урока"
         */
        public static class LessonStopped extends Events
        {

            /**
             * Тип события "Отправка запроса на открытие урока"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.LessonStopped");

            //Параметры события
            public LessonStopped(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public LessonStopped(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static LessonStopped Create(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                LessonStopped e = new LessonStopped(eventSource, eventTarget);

                return e;
            }

            /**
             * Генерация экземпляра события
             * "StudentRejectedNotification".Отправка запроса на открытие урока*
             * @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_GroupLessonOrganization eventSource)
            {
                LessonStopped e = Create(eventSource, eventSource);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Отправка запроса на открытие урока""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Получение уведомления о начале занятия" #####">
        /**
         * Класс экземпляра события "Получение уведомления о начале занятия"
         */
        public static class LessonStarted extends Events
        {

            /**
             * Тип события "Получение уведомления о начале занятия"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.MyEvent1");

            //Параметры события
            public UUID LessonID;
            public UUID TeacherID;

            public LessonStarted(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public LessonStarted(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static LessonStarted Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID lessonID, UUID teacherID)
            {
                LessonStarted e = new LessonStarted(eventSource, eventTarget);
                e.LessonID = lessonID;
                e.TeacherID = teacherID;

                return e;
            }

            /**
             * Генерация экземпляра события
             * "StudentRejectedNotification".Получение уведомления о начале
             * занятия* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_GroupLessonOrganization eventSource, UUID lessonID, UUID teacherID)
            {
                LessonStarted e = Create(eventSource, eventSource, lessonID, teacherID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Получение уведомления о начале занятия""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Студент присоединился к занятию" #####">
        /**
         * Класс экземпляра события "Студент подключился к занятию"
         */
        public static class StudentJoinedLesson extends Events
        {

            /**
             * Тип события "Студент подключился к занятию"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.StudentEnteredLesson");

            //Параметры события
            public UUID LessonID;
            public UUID StudentID;

            //int parametr1;
            //int parametr2;
            public StudentJoinedLesson(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StudentJoinedLesson(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StudentJoinedLesson Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID lessonID, UUID studentID)
            {
                StudentJoinedLesson e = new StudentJoinedLesson(eventSource, eventTarget);
                e.LessonID = lessonID;
                e.StudentID = studentID;

                return e;
            }

            /**
             * Генерация экземпляра события
             * "StudentRejectedNotification".Студент подключился к занятию*
             * @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_GroupLessonOrganization eventSource, UUID lessonID, UUID studentID)
            {
                StudentJoinedLesson e = Create(eventSource, eventSource, lessonID, studentID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Студент подключился к занятию""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Студент отклонил уведомление о начале занятия" #####">
        /**
         * Класс экземпляра события "Студент отклонил уведомление о начале
         * занятия"
         */
        public static class StudentRejectedNotification extends Events
        {

            /**
             * Тип события "Студент отклонил уведомление о начале занятия"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.MyEvent1");

            //Параметры события
            public UUID LessonID;
            public UUID StudentID;

            public StudentRejectedNotification(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StudentRejectedNotification(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StudentRejectedNotification Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID lessonID, UUID studentID)
            {
                StudentRejectedNotification e = new StudentRejectedNotification(eventSource, eventTarget);
                e.LessonID = lessonID;
                e.StudentID = studentID;

                return e;
            }

            /**
             * Генерация экземпляра события
             * "StudentRejectedNotification".Студент отклонил уведомление о
             * начале занятия* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_GroupLessonOrganization eventSource, UUID lessonID, UUID studentID)
            {
                StudentRejectedNotification e = Create(eventSource, eventSource, lessonID, studentID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Студент отклонил уведомление о начале занятия""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Студент покинул занятие" #####">
        /**
         * Класс экземпляра события "Студент покинул занятие"
         */
        public static class StudentLeftLesson extends Events
        {

            /**
             * Тип события "Студент покинул занятие"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.StudentLeftLesson");

            //Параметры события
            public UUID StudentID;

            //int parametr1;
            //int parametr2;
            public StudentLeftLesson(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StudentLeftLesson(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StudentLeftLesson Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID studentID)
            {
                StudentLeftLesson e = new StudentLeftLesson(eventSource, eventTarget);
                e.StudentID = studentID;

                return e;
            }

            /**
             * Генерация экземпляра события
             * "StudentRejectedNotification".Студент подключился к занятию*
             * @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_GroupLessonOrganization eventSource, UUID studentID)
            {
                StudentLeftLesson e = Create(eventSource, eventSource, studentID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Студент покинул занятие""
//##################################################################
        
        //##################################################################
  //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Преподаватель закончил урок" #####">
  /**Класс экземпляра события "Преподаватель закончил урок" */
 public static class TeacherStoppedLesson extends Events
 { 

   /** Тип события "Преподаватель закончил урок" */
  protected final static IATOMName _EventName = new ATOMName("EventSource.Events.TeacherStoppedLesson");
  
  //Параметры события
  public UUID TeacherID;
  //int parametr1;
  //int parametr2;
 
  public TeacherStoppedLesson(IATOMEventDelegateOwner eventSource)
  {
    super(eventSource);
  }
 
  public  TeacherStoppedLesson(IATOMEventDelegateOwner eventSource, Object eventTarget)
  {
    super(eventSource, eventTarget);
  }
  
   @Override
   public IATOMName getEventName()
   {
     return _EventName;
   }
 
  protected static TeacherStoppedLesson Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID teacherID /*, int parametr1, int parametr2*/)
  {
    TeacherStoppedLesson e = new TeacherStoppedLesson(eventSource,eventTarget);
    e.TeacherID = teacherID;
    //e.parametr1 = parametr1;
   // e.parametr2 = parametr2;
 
    return e;
  }
 
  /**Генерация экземпляра события "MyEvent1".Преподаватель закончил урок
    * @param eventSource Источник события.
    * @return true - изменение состояния произведено, false - Иначе
    */
  protected static boolean Rise(Protocol_GroupLessonOrganization eventSource, UUID teacherID /*, int parametr1, int parametr2*/)
  {
    TeacherStoppedLesson e = Create(eventSource,eventSource, teacherID/*,param1, param2*/);
 
    return eventSource.Delegate().Rise(e);
  }  

}
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Преподаватель закончил урок""
//##################################################################
 
 
 
 
 
 

    }; // ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################

    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ATOMNetworkProtocol.EventLogic     //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //    isAllowEvent = OnBefore_Event1(event);
            // }
            //2. Событие Events.Event2
            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (Events.StartLessonRequest.class.isAssignableFrom(eventClass))
            {
                Events.StartLessonRequest event = (Events.StartLessonRequest) e;
                isDone = OnDo_StartLessonRequest(event);
            }

            if (Events.StudentJoinedLesson.class.isAssignableFrom(eventClass))
            {
                Events.StudentJoinedLesson event = (Events.StudentJoinedLesson) e;
                isDone = OnDo_StudentJoinedLesson(event);
            }

            if (Events.StudentRejectedNotification.class.isAssignableFrom(eventClass))
            {
                Events.StudentRejectedNotification event = (Events.StudentRejectedNotification) e;
                isDone = OnDo_StudentRejectedNotification(event);
            }

            if (Events.StudentLeftLesson.class.isAssignableFrom(eventClass))
            {
                Events.StudentLeftLesson event = (Events.StudentLeftLesson) e;
                isDone = OnDo_StudentLeftLesson(event);
            }

            if (Events.LessonStarted.class.isAssignableFrom(eventClass))
            {
                Events.LessonStarted event = (Events.LessonStarted) e;
                isDone = OnDo_LessonStarted(event);
            }
            
            if (Events.LessonStopped.class.isAssignableFrom(eventClass))
            {
                Events.LessonStopped event = (Events.LessonStopped) e;
                isDone = OnDo_LessonStopped(event);
            }

            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //
            // }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
    } //--- END CLASS EventLogic

//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################
}
