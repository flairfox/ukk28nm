/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_GroupLessonOrganization;

import atom.errors.ATOMError;
import atom.names.ATOMStringResource;
import atom.names.IATOMName;
import atom.network.ATOMNetworkCommand;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Vadim
 */
public class Command_JoinLesson extends ATOMNetworkCommand
{

    public static IATOMName COMMAND_NAME = new ATOMStringResource("Command_JoinLesson", "Присоединиться к уроку");
    public static IATOMName STUDENT_ID = new ATOMStringResource("StudentID", "ID студента");
    public static IATOMName LESSON_ID = new ATOMStringResource("LessonID", "ID урока");

    public UUID StudentID;
    public UUID LessonID;

    public Command_JoinLesson(UUID protocolID)
    {
        _CommonBlock.ProtocolID = protocolID;
    }

    @Override
    public IATOMName GetCommandName()
    {
        return COMMAND_NAME;
    }

    @Override
    public void Serialize(JSONObject rawNetworkPackage)
    {
        super.Serialize(rawNetworkPackage);

        try
        {
            // Сериализация полей команды и командых блоков (при наличии)
            rawNetworkPackage.put(STUDENT_ID.LocalName(), StudentID.toString());

            rawNetworkPackage.put(LESSON_ID.LocalName(), LessonID.toString());

        } catch (JSONException ex)
        {
            Logger.getLogger(Command_JoinLesson.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.Serialize(rawNetworkPackage); // записываем базовые данные команды
    }

    @Override
    public ATOMError Deserialize(JSONObject rawNetworkPackage)
    {
        super.Deserialize(rawNetworkPackage); // Расшифровываем базовые данные команды

        StudentID = UUID.fromString(GetStringValue(STUDENT_ID.LocalName()));

        LessonID = UUID.fromString(GetStringValue(LESSON_ID.LocalName()));

        return null;
    }
}
