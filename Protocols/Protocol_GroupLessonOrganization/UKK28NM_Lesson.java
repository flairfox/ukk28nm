package ukk28nm.Protocols.Protocol_GroupLessonOrganization;

import java.util.LinkedList;
import java.util.UUID;

/**
 *
 * @author Добровольский
 */
public class UKK28NM_Lesson
{

    private UUID _LessonID;
    private UUID _TeacherID;
    private LinkedList<UUID> _StudentIDs_MustBePresent;
    private LinkedList<UUID> _StudentIDs_Present;

    public UKK28NM_Lesson(UUID lessonID, UUID teacherID, LinkedList<UUID> mustBePresent)
    {
        _LessonID = lessonID;
        _TeacherID = teacherID;
        
        _StudentIDs_MustBePresent = mustBePresent;
        _StudentIDs_Present = new LinkedList<UUID>();
    }
    
    public UUID getLessonID()
    {
        return _LessonID;
    }
    
    public UUID getTeacherID()
    {
        return _TeacherID;
    }
    
    public LinkedList<UUID> getStudentsIDs_MustBePresent()
    {
        return _StudentIDs_MustBePresent;
    }
    
    public LinkedList<UUID> getStudentsIDs_Present()
    {
        return _StudentIDs_Present;
    }
    public boolean IsStudentMustBeOnLesson(UUID studentID)
    {
       return _StudentIDs_MustBePresent.contains(studentID);
    }

    public void setStudentAsPresent(UUID studentID)
    {
        _StudentIDs_MustBePresent.remove(studentID);
        _StudentIDs_Present.add(studentID);
    }
    
    public void setStudentAsMustBePresent(UUID studentID)
    {
        _StudentIDs_Present.remove(studentID);
        _StudentIDs_MustBePresent.add(studentID);
    }
}
