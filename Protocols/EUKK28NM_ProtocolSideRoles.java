/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols;

import atom.network.ATOMNetworkProtocol;

/**
 *
 * @author Vadim
 */
public class EUKK28NM_ProtocolSideRoles extends ATOMNetworkProtocol.ProtocolSideRole
{

    public static final EUKK28NM_ProtocolSideRoles SERVER = new EUKK28NM_ProtocolSideRoles("Server");
    public static final EUKK28NM_ProtocolSideRoles USER = new EUKK28NM_ProtocolSideRoles("User");
    public static final EUKK28NM_ProtocolSideRoles TEACHER = new EUKK28NM_ProtocolSideRoles("Teacher");
    public static final EUKK28NM_ProtocolSideRoles STUDENT = new EUKK28NM_ProtocolSideRoles("Student");

    public EUKK28NM_ProtocolSideRoles(String roleName)
    {
        super(roleName);
    }
    
    public String getRoleName()
    {
        return RoleName;
    }
}
