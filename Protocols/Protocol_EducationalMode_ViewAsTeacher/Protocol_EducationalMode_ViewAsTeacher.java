/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_EducationalMode_ViewAsTeacher;

import atom.network.ATOMListeningSocket_Abstract;
import atom.network.ATOMNetworkCommand;
import atom.network.ATOMNetworkProtocol;
import atom.network.ATOMSocket_Abstract;
import java.util.LinkedList;
import java.util.UUID;
import ukk28nm.Protocols.Protocol_GroupLessonOrganization.Protocol_GroupLessonOrganization;
import ukk28nm.Protocols.Protocol_UsersRegistration.Protocol_UserRegistration;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogicOwner;
import atom.events2.IATOMEvent;
import atom.names.ATOMName;
import atom.names.IATOMName;
import java.util.HashMap;
import ukk28nm.Protocols.EUKK28NM_ProtocolSideRoles;

/**
 *
 * @author Vadim
 */
public class Protocol_EducationalMode_ViewAsTeacher extends ATOMNetworkProtocol implements IATOMEventDelegateOwner, IATOMEventLogicOwner
{

    protected static UUID PROTOCOL_ID = UUID.fromString("ceeb6540-36d2-48b8-b7cb-1fe383cb9f5e");

    protected ATOMSocket_Abstract _WorkSocket;

    protected Protocol_UserRegistration _UserRegistration;
    protected Protocol_GroupLessonOrganization _GroupLessonOrganization;

    protected boolean _isBroadcasting = false;
    
    protected HashMap<UUID, UUID> _BroadcastingTeachers;

    public Protocol_EducationalMode_ViewAsTeacher(ProtocolSideRole protocolSide, Protocol_UserRegistration userRegistration, Protocol_GroupLessonOrganization groupLessonOrganization, ATOMListeningSocket_Abstract listeningSocket)
    {
        super(protocolSide);
        AddListeningSocket(listeningSocket);

        _UserRegistration = userRegistration;
        _GroupLessonOrganization = groupLessonOrganization;
        _GroupLessonOrganization.Delegate().AddListener(_CommonListener);

        _Delegate = new EventLogic();
        
        _BroadcastingTeachers = new HashMap<UUID, UUID>();
    }

    public Protocol_EducationalMode_ViewAsTeacher(ProtocolSideRole protocolSide, Protocol_UserRegistration userRegistration, Protocol_GroupLessonOrganization groupLessonOrganization, ATOMSocket_Abstract workSocket)
    {
        super(protocolSide);
        AddWorkSocket(workSocket);

        _UserRegistration = userRegistration;
        _GroupLessonOrganization = groupLessonOrganization;

        _WorkSocket = workSocket;

        _Delegate = new EventLogic();
    }

    public void StartBroadcast()
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.TEACHER))
        {
            Command_StartBroadcast commandStartBroadcast = new Command_StartBroadcast(PROTOCOL_ID);

            SendCommand(commandStartBroadcast, _WorkSocket);
            _isBroadcasting = true;
        }
    }

    protected void StartBroadcast(UUID teacherID)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_StartBroadcast commandStartBroadcast = new Command_StartBroadcast(PROTOCOL_ID);

            LinkedList<UUID> studentIDs = _GroupLessonOrganization.GetStudentIDs_Present(teacherID);
            for (UUID studentID : studentIDs)
            {
                SendCommand(commandStartBroadcast, _UserRegistration.FindUserCommunicationSocket(studentID));
            }
            
            _BroadcastingTeachers.put(teacherID, null);
        }
    }

    public void UpdateSection(UUID sectionID)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.TEACHER))
        {
            Command_UpdateSection commandUpdateSection = new Command_UpdateSection(PROTOCOL_ID);
            commandUpdateSection.SectionID = sectionID;

            SendCommand(commandUpdateSection, _WorkSocket);
        }
    }

    protected void UpdateSection(UUID sectionID, UUID teacherID)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_UpdateSection commandUpdateSection = new Command_UpdateSection(PROTOCOL_ID);
            commandUpdateSection.SectionID = sectionID;

            LinkedList<UUID> studentIDs = _GroupLessonOrganization.GetStudentIDs_Present(teacherID);
            for (UUID studentID : studentIDs)
            {
                SendCommand(commandUpdateSection, _UserRegistration.FindUserCommunicationSocket(studentID));
            }
            
            _BroadcastingTeachers.put(teacherID, sectionID);
        }
    }

    public void StopBroadcast()
    {
        if (!_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_StopBroadcast commandStopBroadcast = new Command_StopBroadcast(PROTOCOL_ID);

            SendCommand(commandStopBroadcast, _WorkSocket);
            _isBroadcasting = false;
        }
    }

    protected void StopBroadcast(UUID teacherID)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_StopBroadcast commandStopBroadcast = new Command_StopBroadcast(PROTOCOL_ID);

            LinkedList<UUID> studentIDs = _GroupLessonOrganization.GetStudentIDs_Present(teacherID);
            for (UUID studentID : studentIDs)
            {
                SendCommand(commandStopBroadcast, _UserRegistration.FindUserCommunicationSocket(studentID));
            }
        }
        
        _BroadcastingTeachers.remove(teacherID);
    }

    @Override
    protected boolean OnDo_ProcessIncomingCommand(ATOMNetworkProtocol.Events.IncomingNetworkCommandProcess event)
    {
        ATOMNetworkCommand command = event.IncomingCommand;

        if (command instanceof Command_StartBroadcast)
        { // Команда "Пользователь отправил запрос на начало режима кадр как у меня"
            Command_StartBroadcast incomingCommand = (Command_StartBroadcast) command;

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
            {
                Events.StartBroadcast.Rise(this);
            }

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.StartBroadcastRequest.Rise(this, _UserRegistration.FindUserID(event.SocketToResponceSender));
            }
        }

        if (command instanceof Command_UpdateSection)
        { // Команда "Пользователь отправил запрос на обновление раздела"
            Command_UpdateSection incomingCommand = (Command_UpdateSection) command;

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
            {
                Events.UpdateSection.Rise(this, incomingCommand.SectionID);
            }

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.UpdateSectionRequest.Rise(this, incomingCommand.SectionID, _UserRegistration.FindUserID(event.SocketToResponceSender));
            }
        }

        if (command instanceof Command_StopBroadcast)
        { // Команда "Пользователь отправил запрос на окончание режима кадр как у меня"
            Command_StopBroadcast incomingCommand = (Command_StopBroadcast) command;

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
            {
                Events.StopBroadcast.Rise(this);
            }

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.StopBroadcastRequest.Rise(this, _UserRegistration.FindUserID(event.SocketToResponceSender));
            }
        }

        return true;
    }

    private boolean OnDo_StartBroadcastRequest(Events.StartBroadcastRequest event)
    {
        StartBroadcast(event.TeacherID);

        return true;
    }

    private boolean OnDo_StartBroadcast(Events.StartBroadcast event)
    {
        return true;
    }

    private boolean OnDo_UpdateSectionRequest(Events.UpdateSectionRequest event)
    {
        UpdateSection(event.SectionID, event.TeacherID);

        return true;
    }

    private boolean OnDo_UpdateSection(Events.UpdateSection event)
    {
        return true;
    }

    private boolean OnDo_StopBroadcastRequest(Events.StopBroadcastRequest event)
    {
        StopBroadcast(event.TeacherID);

        return true;
    }

    private boolean OnDo_StopBroadcast(Events.StopBroadcast event)
    {
        return true;
    }

    private boolean OnAfter_StudentJoinedLesson(Protocol_GroupLessonOrganization.Events.StudentJoinedLesson event)
    {
        UUID teacherID = _GroupLessonOrganization.FindTeacherIDByPresentStudentID(event.StudentID);
        
        if (_BroadcastingTeachers.containsKey(teacherID) && _BroadcastingTeachers.get(teacherID) != null)
        {
            Command_StartBroadcast commandStartBroadcast = new Command_StartBroadcast(PROTOCOL_ID);
            SendCommand(commandStartBroadcast, _UserRegistration.FindUserCommunicationSocket(event.StudentID));

            Command_UpdateSection commandUpdateSection = new Command_UpdateSection(PROTOCOL_ID);
            commandUpdateSection.SectionID = _BroadcastingTeachers.get(teacherID);
            SendCommand(commandUpdateSection, _UserRegistration.FindUserCommunicationSocket(event.StudentID));
        }
        
        return true;
    }
    
  private boolean OnAfter_LessonStopped(Protocol_GroupLessonOrganization.Events.TeacherStoppedLesson event)
  {
    if (_BroadcastingTeachers.containsKey(event.TeacherID))
    {
      _BroadcastingTeachers.remove(event.TeacherID);
    }

    return true;
    }

    @Override
    public void Start()
    {
    }

    @Override
    protected ATOMNetworkCommand CreateCommandByName(String commandName)
    {
        if (Command_StartBroadcast.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_StartBroadcast(PROTOCOL_ID);
        }

        if (Command_UpdateSection.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_UpdateSection(PROTOCOL_ID);
        }

        if (Command_StopBroadcast.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_StopBroadcast(PROTOCOL_ID);
        }

        return null;
    }

    @Override
    public UUID getProtocolID()
    {
        return PROTOCOL_ID;
    }

    public boolean isBroadcasting()
    {
        return _isBroadcasting;
    }
    
    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="##### УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ #####">
    protected CommonListener _CommonListener = new CommonListener();
    
    protected class CommonListener extends ATOMNetworkProtocol.CommonListener
    {

        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            //1. Обработка события TEventSource.Events.Event1
            //if (TEventSource.Events.Event1.class.isAssignableFrom(eventClass))
            //{
            //  TEventSource.Events.Event1 event = (TEventSource.Events.Event1) e;
            //  isAllowEvent = OnBefore_Event1(event);
            //}
            return isAllowEvent;
        }
        
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработка события TEventSource.Events.Event1
            //if (TEventSource.Events.Event1.class.isAssignableFrom(eventClass))
            //{
            //  TEventSource.Events.Event1 event = (TEventSource.Events.Event1) e;
            //  isDone  = OnDo_Event1(event);
            // }
            return isDone;
        }
        
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            //1. Обработка события TEventSource.Events.Event1
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            if (Protocol_GroupLessonOrganization.Events.StudentJoinedLesson.class.isAssignableFrom(eventClass))
            {
                Protocol_GroupLessonOrganization.Events.StudentJoinedLesson event = (Protocol_GroupLessonOrganization.Events.StudentJoinedLesson) e;
                isDone = OnAfter_StudentJoinedLesson(event);
            }
            
          if (Protocol_GroupLessonOrganization.Events.TeacherStoppedLesson.class.isAssignableFrom(eventClass))
          {
            Protocol_GroupLessonOrganization.Events.TeacherStoppedLesson event = (Protocol_GroupLessonOrganization.Events.TeacherStoppedLesson) e;
            isDone = OnAfter_LessonStopped(event);
          }
        }
    }
//</editor-fold> КОНЕЦ "УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ"
//##################################################################

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="ТИПЫ СОБЫТИЙ класса EventSource">
    //========================================================
    // --- ТИПЫ СОБЫТИЙ класса EventSource
    public abstract static class Events extends ATOMNetworkProtocol.Events
    {

        protected Events(IATOMEventDelegateOwner eventSource)
        {
            super((ATOMNetworkProtocol) eventSource);
        }

        protected Events(IATOMEventDelegateOwner eventSource, Object eventTarget)
        {
            super(eventSource, eventTarget);
        }

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Запрос на начало режима кадр как у меня" #####">
        /**
         * Класс экземпляра события "Запрос на начало режима кадр как у меня"
         */
        public static class StartBroadcastRequest extends Events
        {

            /**
             * Тип события "Запрос на начало режима кадр как у меня"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.StartBroadcastRequest");

            //Параметры события
            UUID TeacherID;

            //int parametr1;
            //int parametr2;
            public StartBroadcastRequest(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StartBroadcastRequest(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StartBroadcastRequest Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID teacherID)
            {
                StartBroadcastRequest e = new StartBroadcastRequest(eventSource, eventTarget);
                e.TeacherID = teacherID;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Запрос на начало режима
             * кадр как у меня* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_EducationalMode_ViewAsTeacher eventSource, UUID teacherID)
            {
                StartBroadcastRequest e = Create(eventSource, eventSource, teacherID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Запрос на начало режима кадр как у меня""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Начало режима кадр как у меня" #####">
        /**
         * Класс экземпляра события "Начало режима кадр как у меня"
         */
        public static class StartBroadcast extends Events
        {

            /**
             * Тип события "Начало режима кадр как у меня"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.MyEvent1");

            //Параметры события
            //int parametr1;
            //int parametr2;
            public StartBroadcast(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StartBroadcast(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StartBroadcast Create(IATOMEventDelegateOwner eventSource, Object eventTarget /*, int parametr1, int parametr2*/)
            {
                StartBroadcast e = new StartBroadcast(eventSource, eventTarget);
                //e.parametr1 = parametr1;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Начало режима кадр как у
             * меня* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_EducationalMode_ViewAsTeacher eventSource /*, int parametr1, int parametr2*/)
            {
                StartBroadcast e = Create(eventSource, eventSource/*,param1, param2*/);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Начало режима кадр как у меня""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Запрос на обновление раздела" #####">
        /**
         * Класс экземпляра события "Запрос на обновление раздела"
         */
        public static class UpdateSectionRequest extends Events
        {

            /**
             * Тип события "Запрос на обновление раздела"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.UpdateSectionRequest");

            //Параметры события
            UUID SectionID;
            UUID TeacherID;

            //int parametr1;
            //int parametr2;
            public UpdateSectionRequest(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public UpdateSectionRequest(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static UpdateSectionRequest Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID sectionID, UUID teacherID)
            {
                UpdateSectionRequest e = new UpdateSectionRequest(eventSource, eventTarget);
                e.SectionID = sectionID;
                e.TeacherID = teacherID;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Запрос на обновление
             * раздела* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_EducationalMode_ViewAsTeacher eventSource, UUID sectionID, UUID teacherID)
            {
                UpdateSectionRequest e = Create(eventSource, eventSource, sectionID, teacherID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Запрос на обновление раздела""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Обновление раздела" #####">
        /**
         * Класс экземпляра события "Обновление раздела"
         */
        public static class UpdateSection extends Events
        {

            /**
             * Тип события "Обновление раздела"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.UpdateSection");

            //Параметры события
            public UUID SectionID;

            //int parametr1;
            //int parametr2;
            public UpdateSection(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public UpdateSection(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static UpdateSection Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID sectionID)
            {
                UpdateSection e = new UpdateSection(eventSource, eventTarget);
                e.SectionID = sectionID;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Обновление раздела
             *
             *
             * @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_EducationalMode_ViewAsTeacher eventSource, UUID sectionID)
            {
                UpdateSection e = Create(eventSource, eventSource, sectionID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Обновление раздела""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Запрос на окончание режима кадр как у меня" #####">
        /**
         * Класс экземпляра события "Запрос на окончание режима кадр как у меня"
         */
        public static class StopBroadcastRequest extends Events
        {

            /**
             * Тип события "Запрос на окончание режима кадр как у меня"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.StopBroadcastRequest");

            //Параметры события
            UUID TeacherID;

            //int parametr1;
            //int parametr2;
            public StopBroadcastRequest(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StopBroadcastRequest(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StopBroadcastRequest Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID teacherID)
            {
                StopBroadcastRequest e = new StopBroadcastRequest(eventSource, eventTarget);
                e.TeacherID = teacherID;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Запрос на окончание
             * режима кадр как у меня* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_EducationalMode_ViewAsTeacher eventSource, UUID teacherID)
            {
                StopBroadcastRequest e = Create(eventSource, eventSource, teacherID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Запрос на окончание режима кадр как у меня""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Окончание режима кадр как у меня" #####">
        /**
         * Класс экземпляра события "Окончание режима кадр как у меня"
         */
        public static class StopBroadcast extends Events
        {

            /**
             * Тип события "Окончание режима кадр как у меня"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.StopBroadcast");

            //Параметры события
            //int parametr1;
            //int parametr2;
            public StopBroadcast(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public StopBroadcast(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static StopBroadcast Create(IATOMEventDelegateOwner eventSource, Object eventTarget /*, int parametr1, int parametr2*/)
            {
                StopBroadcast e = new StopBroadcast(eventSource, eventTarget);
                //e.parametr1 = parametr1;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Окончание режима кадр как
             * у меня* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_EducationalMode_ViewAsTeacher eventSource /*, int parametr1, int parametr2*/)
            {
                StopBroadcast e = Create(eventSource, eventSource/*,param1, param2*/);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Окончание режима кадр как у меня""
//##################################################################
    }; // ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################

    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ATOMNetworkProtocol.EventLogic        //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //    isAllowEvent = OnBefore_Event1(event);
            // }
            //2. Событие Events.Event2
            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (Events.StartBroadcastRequest.class.isAssignableFrom(eventClass))
            {
                Events.StartBroadcastRequest event = (Events.StartBroadcastRequest) e;
                isDone = OnDo_StartBroadcastRequest(event);
            }

            if (Events.StartBroadcast.class.isAssignableFrom(eventClass))
            {
                Events.StartBroadcast event = (Events.StartBroadcast) e;
                isDone = OnDo_StartBroadcast(event);
            }

            if (Events.UpdateSectionRequest.class.isAssignableFrom(eventClass))
            {
                Events.UpdateSectionRequest event = (Events.UpdateSectionRequest) e;
                isDone = OnDo_UpdateSectionRequest(event);
            }

            if (Events.UpdateSection.class.isAssignableFrom(eventClass))
            {
                Events.UpdateSection event = (Events.UpdateSection) e;
                isDone = OnDo_UpdateSection(event);
            }

            if (Events.StopBroadcastRequest.class.isAssignableFrom(eventClass))
            {
                Events.StopBroadcastRequest event = (Events.StopBroadcastRequest) e;
                isDone = OnDo_StopBroadcastRequest(event);
            }

            if (Events.StopBroadcast.class.isAssignableFrom(eventClass))
            {
                Events.StopBroadcast event = (Events.StopBroadcast) e;
                isDone = OnDo_StopBroadcast(event);
            }

            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //
            // }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
    } //--- END CLASS EventLogic

//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################
}
