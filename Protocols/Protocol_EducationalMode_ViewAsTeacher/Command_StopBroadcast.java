/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_EducationalMode_ViewAsTeacher;

import atom.errors.ATOMError;
import atom.names.ATOMStringResource;
import atom.names.IATOMName;
import atom.network.ATOMNetworkCommand;
import java.util.UUID;
import org.json.JSONObject;

/**
 *
 * @author Vadim
 */
public class Command_StopBroadcast extends ATOMNetworkCommand
{

    public static IATOMName COMMAND_NAME = new ATOMStringResource("Command_StopBroadcast", "Отправка запроса на остановку режима показа кадра как у меня");

    public Command_StopBroadcast(UUID protocolID)
    {
        _CommonBlock.ProtocolID = protocolID;
    }

    @Override
    public IATOMName GetCommandName()
    {
        return COMMAND_NAME;
    }

    @Override
    public void Serialize(JSONObject rawNetworkPackage)
    {
        super.Serialize(rawNetworkPackage); // записываем базовые данные команды
    }

    @Override
    public ATOMError Deserialize(JSONObject rawNetworkPackage)
    {
        super.Deserialize(rawNetworkPackage); // Расшифровываем базовые данные команды
        
        return null;
    }
}
