/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_EducationalMode_ViewAsTeacher;

import atom.errors.ATOMError;
import atom.names.ATOMStringResource;
import atom.names.IATOMName;
import atom.network.ATOMNetworkCommand;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Vadim
 */
public class Command_UpdateSection extends ATOMNetworkCommand
{

    public static IATOMName COMMAND_NAME = new ATOMStringResource("Command_UpdateSection", "Обновление раздела, просматриваемого студентом");
    public static IATOMName SECTION_ID = new ATOMStringResource("SectionID", "ID раздела");

    public UUID SectionID;

    public Command_UpdateSection(UUID protocolID)
    {
        _CommonBlock.ProtocolID = protocolID;
    }

    @Override
    public IATOMName GetCommandName()
    {
        return COMMAND_NAME;
    }

    @Override
    public void Serialize(JSONObject rawNetworkPackage)
    {
        super.Serialize(rawNetworkPackage);

        try
        {
            // Сериализация полей команды и командых блоков (при наличии)
            rawNetworkPackage.put(SECTION_ID.LocalName(), SectionID.toString());

        } catch (JSONException ex)
        {
            Logger.getLogger(Command_UpdateSection.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.Serialize(rawNetworkPackage); // записываем базовые данные команды
    }

    @Override
    public ATOMError Deserialize(JSONObject rawNetworkPackage)
    {
        super.Deserialize(rawNetworkPackage); // Расшифровываем базовые данные команды

        SectionID = UUID.fromString(GetStringValue(SECTION_ID.LocalName()));

        return null;
    }
}
