/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_EducationalMode_SectionsAdmittance;

import atom.network.ATOMListeningSocket_Abstract;
import atom.network.ATOMNetworkCommand;
import atom.network.ATOMNetworkProtocol;
import atom.network.ATOMSocket_Abstract;
import java.util.UUID;
import ukk28nm.Protocols.Protocol_GroupLessonOrganization.Protocol_GroupLessonOrganization;
import ukk28nm.Protocols.Protocol_UsersRegistration.Protocol_UserRegistration;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogicOwner;
import atom.events2.IATOMEvent;
import atom.names.ATOMName;
import atom.names.IATOMName;
import ukk28nm.Protocols.EUKK28NM_ProtocolSideRoles;

/**
 *
 * @author Vadim
 */
public class Protocol_EducationalMode_SectionsAdmittance extends ATOMNetworkProtocol implements IATOMEventDelegateOwner, IATOMEventLogicOwner
{

    protected static UUID PROTOCOL_ID = UUID.fromString("fc022200-6706-42ad-9d2d-4c0e74746e3d");

    public enum ESectionAdmittance
    {
        PERMIT, FORBID;
    }

    protected ATOMSocket_Abstract _WorkSocket;

    protected Protocol_UserRegistration _UserRegistration;
    protected Protocol_GroupLessonOrganization _GroupLessonOrganization;

    public Protocol_EducationalMode_SectionsAdmittance(ProtocolSideRole protocolSide, Protocol_UserRegistration userRegistration, Protocol_GroupLessonOrganization groupLessonOrganization, ATOMListeningSocket_Abstract listeningSocket)
    {
        super(protocolSide);
        AddListeningSocket(listeningSocket);

        _UserRegistration = userRegistration;
        _GroupLessonOrganization = groupLessonOrganization;

        _Delegate = new EventLogic();
    }

    public Protocol_EducationalMode_SectionsAdmittance(ProtocolSideRole protocolSide, Protocol_UserRegistration userRegistration, Protocol_GroupLessonOrganization groupLessonOrganization, ATOMSocket_Abstract workSocket)
    {
        super(protocolSide);
        AddWorkSocket(workSocket);

        _UserRegistration = userRegistration;
        _GroupLessonOrganization = groupLessonOrganization;

        _WorkSocket = workSocket;

        _Delegate = new EventLogic();
    }

    public void SetSectionAdmittance(UUID studentID, UUID sectionID, ESectionAdmittance flag)
    {
        Command_SectionAdmittance sectionAdmittanceCommand = new Command_SectionAdmittance(PROTOCOL_ID);
        sectionAdmittanceCommand.StudentID = studentID;
        sectionAdmittanceCommand.SectionID = sectionID;
        sectionAdmittanceCommand.Flag = flag;

        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.TEACHER))
        {
            SendCommand(sectionAdmittanceCommand, _WorkSocket);
        }

        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            SendCommand(sectionAdmittanceCommand, _UserRegistration.FindUserCommunicationSocket(studentID));
        }
    }

    @Override
    protected boolean OnDo_ProcessIncomingCommand(ATOMNetworkProtocol.Events.IncomingNetworkCommandProcess event)
    {
        ATOMNetworkCommand command = event.IncomingCommand;

        if (command instanceof Command_SectionAdmittance)
        { // Команда "Пользователь отправил запрос на открытие урока"
            Command_SectionAdmittance incomingCommand = (Command_SectionAdmittance) command;

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.STUDENT))
            {
                Events.SetSectionAdmittance.Rise(this,
                        incomingCommand.StudentID,
                        incomingCommand.SectionID,
                        incomingCommand.Flag);
            }

            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.SetSectionAdmittanceRequest.Rise(this, _UserRegistration.FindUserID(event.SocketToResponceSender),
                        incomingCommand.StudentID,
                        incomingCommand.SectionID,
                        incomingCommand.Flag);
            }
        }

        return true;
    }

    private boolean OnDo_SetSectionAdmittanceRequest(Events.SetSectionAdmittanceRequest event)
    {
        SetSectionAdmittance(event.StudentID, event.SectionID, event.Flag);

        return true;
    }

    private boolean OnDo_SetSectionAdmittance(Events.SetSectionAdmittance event)
    {
        return true;
    }

    @Override
    public void Start()
    {
       
    }

    @Override
    protected ATOMNetworkCommand CreateCommandByName(String commandName)
    {
        if (Command_SectionAdmittance.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_SectionAdmittance(PROTOCOL_ID);
        }

        return null;
    }

    @Override
    public UUID getProtocolID()
    {
        return PROTOCOL_ID;
    }

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="ТИПЫ СОБЫТИЙ класса EventSource">
    //========================================================
    // --- ТИПЫ СОБЫТИЙ класса EventSource
    public abstract static class Events extends ATOMNetworkProtocol.Events
    {

        protected Events(IATOMEventDelegateOwner eventSource)
        {
            super((ATOMNetworkProtocol) eventSource);
        }

        protected Events(IATOMEventDelegateOwner eventSource, Object eventTarget)
        {
            super(eventSource, eventTarget);
        }

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 1
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Запрос на изменение прав доступа к разделу" #####">
        /**
         * Класс экземпляра события "Запрос на изменение прав доступа к разделу"
         */
        public static class SetSectionAdmittanceRequest extends Events
        {

            /**
             * Тип события "Запрос на изменение прав доступа к разделу"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.SetSectionAdmittanceRequest");

            //Параметры события
            public UUID TeacherID;
            public UUID StudentID;
            public UUID SectionID;
            public ESectionAdmittance Flag;

            public SetSectionAdmittanceRequest(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public SetSectionAdmittanceRequest(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static SetSectionAdmittanceRequest Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID teacherID, UUID studentID, UUID sectionID, ESectionAdmittance flag)
            {
                SetSectionAdmittanceRequest e = new SetSectionAdmittanceRequest(eventSource, eventTarget);
                e.TeacherID = teacherID;
                e.StudentID = studentID;
                e.SectionID = sectionID;
                e.Flag = flag;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Запрос на изменение прав
             * доступа к разделу* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_EducationalMode_SectionsAdmittance eventSource, UUID teacherID, UUID studentID, UUID sectionID, ESectionAdmittance flag)
            {
                SetSectionAdmittanceRequest e = Create(eventSource, eventSource, teacherID, studentID, sectionID, flag);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Запрос на изменение прав доступа к разделу""
//##################################################################

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 2
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Изменение прав доступа к раздела" #####">
        /**
         * Класс экземпляра события "Изменение прав доступа к раздела"
         */
        public static class SetSectionAdmittance extends Events
        {

            /**
             * Тип события "Изменение прав доступа к раздела"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.SetSectionAdmittance");

            //Параметры события
            public UUID StudentID;
            public UUID SectionID;
            public ESectionAdmittance Flag;

            public SetSectionAdmittance(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public SetSectionAdmittance(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static SetSectionAdmittance Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID studentID, UUID sectionID, ESectionAdmittance flag)
            {
                SetSectionAdmittance e = new SetSectionAdmittance(eventSource, eventTarget);
                e.StudentID = studentID;
                e.SectionID = sectionID;
                e.Flag = flag;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Изменение прав доступа к
             * раздела* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_EducationalMode_SectionsAdmittance eventSource, UUID studentID, UUID sectionID, ESectionAdmittance flag)
            {
                SetSectionAdmittance e = Create(eventSource, eventSource, studentID, sectionID, flag);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Изменение прав доступа к раздела""
//##################################################################

    }; // ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################

    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ATOMNetworkProtocol.EventLogic        //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //    isAllowEvent = OnBefore_Event1(event);
            // }
            //2. Событие Events.Event2
            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (Events.SetSectionAdmittanceRequest.class.isAssignableFrom(eventClass))
            {
                Events.SetSectionAdmittanceRequest event = (Events.SetSectionAdmittanceRequest) e;
                isDone = OnDo_SetSectionAdmittanceRequest(event);
            }

            if (Events.SetSectionAdmittance.class.isAssignableFrom(eventClass))
            {
                Events.SetSectionAdmittance event = (Events.SetSectionAdmittance) e;
                isDone = OnDo_SetSectionAdmittance(event);
            }
            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //
            // }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
    } //--- END CLASS EventLogic

//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################
}
