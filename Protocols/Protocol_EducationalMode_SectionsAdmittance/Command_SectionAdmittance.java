/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_EducationalMode_SectionsAdmittance;

import atom.errors.ATOMError;
import atom.names.ATOMStringResource;
import atom.names.IATOMName;
import atom.network.ATOMNetworkCommand;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Vadim
 */
public class Command_SectionAdmittance extends ATOMNetworkCommand
{

    public static IATOMName COMMAND_NAME = new ATOMStringResource("Command_SectionAdmittance", "Изменение права доступа к расзделу студентом");
    public static IATOMName STUDENT_ID = new ATOMStringResource("StudentID", "ID студента");
    public static IATOMName SECTION_ID = new ATOMStringResource("SectionID", "ID раздела");
    public static IATOMName FLAG = new ATOMStringResource("Flag", "Модификатор доступа (permit - forbid)");

    public UUID StudentID;
    public UUID SectionID;
    public Protocol_EducationalMode_SectionsAdmittance.ESectionAdmittance Flag;

    public Command_SectionAdmittance(UUID protocolID)
    {
        _CommonBlock.ProtocolID = protocolID;
    }

    @Override
    public IATOMName GetCommandName()
    {
        return COMMAND_NAME;
    }

    @Override
    public void Serialize(JSONObject rawNetworkPackage)
    {
        super.Serialize(rawNetworkPackage);

        try
        {
            // Сериализация полей команды и командых блоков (при наличии)
            rawNetworkPackage.put(STUDENT_ID.LocalName(), StudentID.toString());
            
            rawNetworkPackage.put(SECTION_ID.LocalName(), SectionID.toString());
            
            rawNetworkPackage.put(FLAG.LocalName(), Flag.toString());

        } catch (JSONException ex)
        {
            Logger.getLogger(Command_SectionAdmittance.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.Serialize(rawNetworkPackage); // записываем базовые данные команды
    }

    @Override
    public ATOMError Deserialize(JSONObject rawNetworkPackage)
    {
        super.Deserialize(rawNetworkPackage); // Расшифровываем базовые данные команды

        StudentID = UUID.fromString(GetStringValue(STUDENT_ID.LocalName()));
        
        SectionID = UUID.fromString(GetStringValue(SECTION_ID.LocalName()));
        
        String sectionAdmittance = GetStringValue(FLAG.LocalName());
        if(sectionAdmittance.equals("PERMIT"))
        {
            Flag = Protocol_EducationalMode_SectionsAdmittance.ESectionAdmittance.PERMIT;
        } else if(sectionAdmittance.equals("FORBID"))
        {
            Flag = Protocol_EducationalMode_SectionsAdmittance.ESectionAdmittance.FORBID;
        }

        return null;
    }
}
