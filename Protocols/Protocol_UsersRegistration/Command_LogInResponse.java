package ukk28nm.Protocols.Protocol_UsersRegistration;

import atom.errors.ATOMError;
import atom.names.ATOMStringResource;
import atom.names.IATOMName;
import atom.network.ATOMNetworkCommand;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author Vadim
 */
public class Command_LogInResponse extends ATOMNetworkCommand
{

    public static IATOMName COMMAND_NAME = new ATOMStringResource("Command_LogInResponse", "Результат регистрации ползователя");
    public static IATOMName RESPONSE_TEXT = new ATOMStringResource("ResponseText", "Текст ответа от сервера");
    
    public String ResponseText;

    public Command_LogInResponse(UUID protocolID)
    {
        _CommonBlock.ProtocolID = protocolID;
    }

    @Override
    public IATOMName GetCommandName()
    {
        return COMMAND_NAME;
    }

    @Override
    public void Serialize(JSONObject rawNetworkPackage)
    {
        super.Serialize(rawNetworkPackage);

        try
        {
            // Сериализация полей команды и командых блоков (при наличии)
            rawNetworkPackage.put(RESPONSE_TEXT.LocalName(), ResponseText);

        } catch (JSONException ex)
        {
            Logger.getLogger(Command_LogInResponse.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.Serialize(rawNetworkPackage); // записываем базовые данные команды
    }

    @Override
    public ATOMError Deserialize(JSONObject rawNetworkPackage)
    {
        super.Deserialize(rawNetworkPackage); // Расшифровываем базовые данные команды

        ResponseText = GetStringValue(RESPONSE_TEXT.LocalName());

        return null;
    }
}
