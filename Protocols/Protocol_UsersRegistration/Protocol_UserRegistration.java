/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_UsersRegistration;

import atom.events2.IATOMEvent;
import atom.events2.IATOMEventDelegateOwner;
import atom.events2.IATOMEventLogicOwner;
import atom.names.ATOMName;
import atom.names.IATOMName;
import atom.network.ATOMListeningSocket_Abstract;
import atom.network.ATOMNetworkCommand;
import atom.network.ATOMNetworkProtocol;
import atom.network.ATOMSocket_Abstract;
import java.util.HashMap;
import java.util.Set;
import java.util.UUID;
import ukk28nm.Protocols.EUKK28NM_ProtocolSideRoles;

/**
 *
 * @author Vadim
 */
public class Protocol_UserRegistration extends ATOMNetworkProtocol implements IATOMEventDelegateOwner, IATOMEventLogicOwner
{


    protected static UUID PROTOCOL_ID = UUID.fromString("6a2dcd3f-24f6-4d34-abea-d77cd9e2014d");

    /**
     * Сокет, используемый протоколом в роли ServerSide для ожидания новых
     * внешних подключений
     */
    protected ATOMListeningSocket_Abstract _ListeningSocket;

    /**
     * Сокет, используемый протоколом в роли ClientSide для передачи сообщений
     * на сервер
     */
    public ATOMSocket_Abstract _WorkSocket;

    /**
     * Карта подключенных пользователей. Содержит два уровня: первый разделяет
     * пользователей по статусу (преподаватель, студент), второй привязывает
     * сокет, по которому идет обмен данными с данным пользователем к его ID
     * (регистрационное имя)
     */
    protected HashMap<EUKK28NM_ProtocolSideRoles, HashMap<UUID, ATOMSocket_Abstract>> _UserMap;

    /**
     * Конструктор протокола регистрации подключенных пользователей
         * @param protocolSide Принимает значения EUKK28NM_ProtocolSideRoles.SERVER, EUKK28NM_ProtocolSideRoles.USER
     * @param listeningSocket 
     */
    public Protocol_UserRegistration(EUKK28NM_ProtocolSideRoles protocolSide, ATOMListeningSocket_Abstract listeningSocket)
    {
        super(protocolSide);
        _ListeningSocket = listeningSocket;
        AddListeningSocket(listeningSocket);
        
        _Delegate = new EventLogic();

        _UserMap = new HashMap<EUKK28NM_ProtocolSideRoles, HashMap<UUID, ATOMSocket_Abstract>>();
        _UserMap.put(EUKK28NM_ProtocolSideRoles.TEACHER, new HashMap<UUID, ATOMSocket_Abstract>());
        _UserMap.put(EUKK28NM_ProtocolSideRoles.STUDENT, new HashMap<UUID, ATOMSocket_Abstract>());
    }

    public Protocol_UserRegistration(ATOMNetworkProtocol.ProtocolSideRole protocolSide, ATOMSocket_Abstract workSocket)
    {
        super(protocolSide);
        _WorkSocket = workSocket;
        AddWorkSocket(workSocket);
        
        _Delegate = new EventLogic();
    }

    @Override
    public void Start()
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            StartAsServer();
            System.out.println("Started as server!");
        } else
        {
            StartAsUser();
            System.out.println("Started as user!");
        }
    }

    @Override
    protected boolean OnDoHandler_ListeningSocket_RemoteSideCloseConnection(ATOMListeningSocket_Abstract.Events.RemoteSideCloseConnection event)
    {
        return super.OnDoHandler_ListeningSocket_RemoteSideCloseConnection(event);
    }

    //------------------------------------------------------------------------------------
    /**
     * Рабочий(клиентский) сокет обнаружил закрытие соединения по ранее
     * установленному соединению
     *
     * @param event
     * @return true - событие обработано успешно, false - иначе
     */
    @Override
    protected boolean OnDoHandler_WorkSocket_RemoteSideCloseConnection(ATOMSocket_Abstract.Events.RemoteSideCloseConnection event)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            RemoveUser(FindUserID(event.DisconnectedSocket));
        } else
        {
            Events.UserDisconnected.Rise(this, FindUserID(event.DisconnectedSocket));
        }

        return super.OnDoHandler_WorkSocket_RemoteSideCloseConnection(event);
    }

    @Override
    protected boolean OnDo_ProcessIncomingCommand(ATOMNetworkProtocol.Events.IncomingNetworkCommandProcess event)
    {

        ATOMNetworkCommand command = event.IncomingCommand;

        if (command instanceof Command_LogInRequest)
        { // Команда "Пользователь отправил свои регистрационные данные"

            Events.LogInRequest.Rise(this, event.SocketToResponceSender, ((Command_LogInRequest) command).UserID, ((Command_LogInRequest) command).UserRole);
        }

        if (command instanceof Command_LogInResponse)
        { // Команда "Ответить ползователю на запрос регистрации"

            //System.out.println(((Command_RegistrationRequest) command).UserID);
            Events.LogInResponce.Rise(this, ((Command_LogInResponse) command).ResponseText);
        }

        if (command instanceof Command_LogOffRequest)
        { // Команда "Ответить ползователю на запрос регистрации"

            //System.out.println(((Command_RegistrationRequest) command).UserID);
            if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
            {
                Events.UserLoggedOff.Rise(this, FindUserID(event.SocketToResponceSender));
            }
        }

        return true;
    }

    @Override
    protected ATOMNetworkCommand CreateCommandByName(String commandName)
    {
        if (Command_LogInRequest.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_LogInRequest(PROTOCOL_ID);
        }

        if (Command_LogInResponse.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_LogInResponse(PROTOCOL_ID);
        }

        if (Command_LogOffRequest.COMMAND_NAME.LocalName().equalsIgnoreCase(commandName))
        {
            return new Command_LogOffRequest(PROTOCOL_ID);
        }

        return null;
    }

    private void StartAsServer()
    {
        if (_ListeningSocket == null)
        {
            return;
        }
    }

    private void StartAsUser()
    {
        if (_WorkSocket == null)
        {
            return;
        }
    }

    private boolean OnDo_LogInRequest(Events.LogInRequest event)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            for (ProtocolSideRole protocolRole : _UserMap.keySet())
            {
                if (_UserMap.get(protocolRole).containsKey(event.UserID))
                {
                    SendLogInResponse("User with this ID is already in the system.", event.CommunicationSocket);
                    System.out.println("User with this ID is already in the system.");
                    return true;
                }
            }

            _UserMap.get(event.UserRole).put(event.UserID, event.CommunicationSocket);
            System.out.println("New user logged in. UserRole: " + event.UserRole.RoleName +  ". UserID: " + event.UserID.toString());
            //SendRegistrationResponse("You successfully registered as " + event.UserID.toString(), event.CommunicationSocket);
        
            Events.UserLoggedIn.Rise(this, event.UserID);
        }
        
        return true;
    }

    private boolean OnDo_LogInResponce(Events.LogInResponce event)
    {
        return true;
    }

    private boolean OnDo_UserLoggedIn(Events.UserLoggedIn event)
    {
        return true;
    }

    private boolean OnDo_UserLoggedOff(Events.UserLoggedOff event)
    {
        RemoveUser(event.UserID);

        return true;
    }

    private void OnAfter_UserLoggedIn(Events.UserLoggedIn event) {
        
    }

    private boolean OnDo_UserDisconnected(Events.UserDisconnected event)
    {
        return true;
    }

    protected void RemoveUser(ATOMSocket_Abstract comunicationSocket)
    {
        for (EUKK28NM_ProtocolSideRoles userRole : _UserMap.keySet())
        {
            for (UUID userID : _UserMap.get(userRole).keySet())
            {
                if (_UserMap.get(userRole).get(userID) == comunicationSocket)
                {
                    _UserMap.get(userRole).remove(userID);
                    System.out.println("User logged off. UserID: " + userID.toString());
                    break;
                }
            }
        }
    }
    
    protected void RemoveUser(UUID userID)
    {
        for (EUKK28NM_ProtocolSideRoles userRole : _UserMap.keySet())
        {
            if(_UserMap.get(userRole).containsKey(userID))
            {
                _UserMap.get(userRole).remove(userID);
                System.out.println("User logged off. UserID: " + userID.toString());
                break;
            }
        }
    }

    public UUID FindUserID(ATOMSocket_Abstract comunicationSocket)
    {
        if (_UserMap == null)
        {
            return null;
        }
        
        for (ProtocolSideRole userRole : _UserMap.keySet())
        {
            for (UUID userID : _UserMap.get(userRole).keySet())
            {
                if (_UserMap.get(userRole).get(userID) == comunicationSocket)
                {
                    return userID;
                }
            }

        }

        return null;
    }

    public ProtocolSideRole GetUserRole(UUID userID)
    {
        for (ProtocolSideRole userRole : _UserMap.keySet())
        {
            if (_UserMap.get(userRole).containsKey(userID))
            {
                return userRole;
            }
        }

        return null;
    }

    protected void SendLogInResponse(String responseText, ATOMSocket_Abstract communicationSocket)
    {
        if (_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_LogInResponse registrationResponse = new Command_LogInResponse(PROTOCOL_ID);
            registrationResponse.ResponseText = responseText;

            SendCommand(registrationResponse, communicationSocket);
        }
    }

    public ATOMSocket_Abstract FindUserCommunicationSocket(UUID userID)
    {
        for (ProtocolSideRole userRole : _UserMap.keySet())
        {
            if (_UserMap.get(userRole).containsKey(userID))
            {
                return _UserMap.get(userRole).get(userID);
            }

        }
        return null;
    }

    public void RequestRegistration(UUID userID, EUKK28NM_ProtocolSideRoles userRole)
    {
        if (!_ProtocolRole.equals(EUKK28NM_ProtocolSideRoles.SERVER))
        {
            Command_LogInRequest userRegistration = new Command_LogInRequest(PROTOCOL_ID);
            userRegistration.UserID = userID;
            userRegistration.UserRole = userRole;

            SendCommand(userRegistration, _WorkSocket);
        }
    }
    
    public void LogOff()
    {
        Command_LogOffRequest logOffRequest = new Command_LogOffRequest(PROTOCOL_ID);
        SendCommand(logOffRequest, _WorkSocket);
    }

    public HashMap<EUKK28NM_ProtocolSideRoles, HashMap<UUID, ATOMSocket_Abstract>> getRegisteredUsers()
    {
        return _UserMap;
    }

    public Set<UUID> getRegisteredTeachers()
    {
        return _UserMap.get(EUKK28NM_ProtocolSideRoles.TEACHER).keySet();
    }

    public Set<UUID> getRegisteredStudents()
    {
        return _UserMap.get(EUKK28NM_ProtocolSideRoles.STUDENT).keySet();
    }

    @Override
    public UUID getProtocolID()
    {
        return PROTOCOL_ID;
    }

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="ТИПЫ СОБЫТИЙ класса EventSource">
    /**
     * Делегат событий класса
     */
    //========================================================
    // --- ТИПЫ СОБЫТИЙ класса EventSource
    public abstract static class Events extends ATOMNetworkProtocol.Events
    {

        protected Events(IATOMEventDelegateOwner eventSource)
        {
            super((ATOMNetworkProtocol) eventSource);
        }

        protected Events(IATOMEventDelegateOwner eventSource, Object eventTarget)
        {
            super(eventSource, eventTarget);
        }

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 1
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Получен запрос на регистрацию нового пользователя" #####">
        /**
         * Класс экземпляра события "В системе зарегистрирован новый
         * пользователь"
         */
        public static class LogInRequest extends Events
        {
            /**
             * Тип события "В системе зарегистрирован новый пользователь"
             */
            private final static IATOMName _EventName = new ATOMName("EventSource.Events.UserRegistered");

            //Параметры события
            //int parametr1;
            public ATOMSocket_Abstract CommunicationSocket;
            public UUID UserID;
            public ProtocolSideRole UserRole;

            public LogInRequest(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public LogInRequest(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static LogInRequest Create(IATOMEventDelegateOwner eventSource, Object eventTarget, ATOMSocket_Abstract communicationSocket, UUID userID, ProtocolSideRole userRole)
            {
                LogInRequest e = new LogInRequest(eventSource, eventTarget);
                e.CommunicationSocket = communicationSocket;
                e.UserID = userID;
                e.UserRole = userRole;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".В системе зарегистрирован
             * новый пользователь* @param eventSource Источник события.
             *
             * @param eventSource
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_UserRegistration eventSource, ATOMSocket_Abstract communicationSocket, UUID userID, ProtocolSideRole userRole)
            {
                LogInRequest e = Create(eventSource, eventSource, communicationSocket, userID, userRole);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "В системе зарегистрирован новый пользователь""
//##################################################################

        //---------------------
        //ЭКЗЕМПЛЯР СОБЫТИЯ 2
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Получен ответ на регистрацию пользователя" #####">
        /**
         * Класс экземпляра события "Получен ответ на регистрацию пользователя"
         */
        public static class LogInResponce extends Events
        {

            /**
             * Тип события "Получен ответ на регистрацию пользователя"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.RegistrationResponce");

            //Параметры события
            String ResponseText;

            //int parametr1;
            //int parametr2;
            public LogInResponce(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public LogInResponce(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static LogInResponce Create(IATOMEventDelegateOwner eventSource, Object eventTarget, String responseText)
            {
                LogInResponce e = new LogInResponce(eventSource, eventTarget);
                e.ResponseText = responseText;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Получен ответ на
             * регистрацию пользователя* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_UserRegistration eventSource, String responseText)
            {
                LogInResponce e = Create(eventSource, eventSource, responseText);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Получен ответ на регистрацию пользователя""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Пользователь отключился" #####">
        /**
         * Класс экземпляра события "Пользователь отключился"
         */
        public static class UserDisconnected extends Events
        {

            /**
             * Тип события "Пользователь отключился"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.UserDisconnected");

            //Параметры события
            UUID UserID;

            //int parametr1;
            //int parametr2;
            public UserDisconnected(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public UserDisconnected(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static UserDisconnected Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID userID)
            {
                UserDisconnected e = new UserDisconnected(eventSource, eventTarget);
                e.UserID = userID;
                //e.parametr1 = parametr1;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "UserDisconnected".Пользователь
             * отключился
             *
             *
             * @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_UserRegistration eventSource, UUID userID)
            {
                UserDisconnected e = Create(eventSource, eventSource, userID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Пользователь отключился""
//##################################################################

        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Пользователь подключился" #####">
        /**
         * Класс экземпляра события "Пользователь подключился"
         */
        public static class UserLoggedIn extends Events
        {

            /**
             * Тип события "Пользователь подключился"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.UserConnected");

            //Параметры события
            public UUID UserID;

            //int parametr1;
            //int parametr2;
            public UserLoggedIn(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }

            public UserLoggedIn(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }

            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }

            protected static UserLoggedIn Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID userID)
            {
                UserLoggedIn e = new UserLoggedIn(eventSource, eventTarget);
                e.UserID = userID;
                //e.parametr1 = parametr1;
                // e.parametr2 = parametr2;

                return e;
            }

            /**
             * Генерация экземпляра события "UserConnected".Пользователь
             * подключился* @param eventSource Источник события.
             *
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_UserRegistration eventSource, UUID userID)
            {
                UserLoggedIn e = Create(eventSource, eventSource, userID);

                return eventSource.Delegate().Rise(e);
            }

        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Пользователь подключился""
//##################################################################
        
        //##################################################################
        //<editor-fold defaultstate="collapsed" desc="##### СОБЫТИЕ "Описание события" #####">
        /**
         * Класс экземпляра события "Описание события"
         */
        public static class UserLoggedOff extends Events
        {

            /**
             * Тип события "Описание события"
             */
            protected final static IATOMName _EventName = new ATOMName("EventSource.Events.MyEvent1");

            //Параметры события
            //int parametr1;
            //int parametr2;
            public UUID UserID;
            
            public UserLoggedOff(IATOMEventDelegateOwner eventSource)
            {
                super(eventSource);
            }
            
            public UserLoggedOff(IATOMEventDelegateOwner eventSource, Object eventTarget)
            {
                super(eventSource, eventTarget);
            }
            
            @Override
            public IATOMName getEventName()
            {
                return _EventName;
            }
            
            protected static UserLoggedOff Create(IATOMEventDelegateOwner eventSource, Object eventTarget, UUID userID /*, int parametr1, int parametr2*/)
            {
                UserLoggedOff e = new UserLoggedOff(eventSource, eventTarget);
                e.UserID = userID;
                //e.parametr1 = parametr1;
                // e.parametr2 = parametr2;
                
                return e;
            }

            /**
             * Генерация экземпляра события "MyEvent1".Описание события
             *
             * @param eventSource Источник события.
             * @return true - изменение состояния произведено, false - Иначе
             */
            protected static boolean Rise(Protocol_UserRegistration eventSource, UUID userID /*, int parametr1, int parametr2*/)
            {
                UserLoggedOff e = Create(eventSource, eventSource, userID/*,param1, param2*/);
                
                return eventSource.Delegate().Rise(e);
            }            
            
        }
//</editor-fold> КОНЕЦ "СОБЫТИЕ "Описание события""
//##################################################################
        


    }; // ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################// ---ТИПЫ СОБЫТИЙ класса EventSource
    //========================================================
    //</editor-fold>
    //##################################################################

    //##################################################################
//<editor-fold defaultstate="collapsed" desc="##### ЛОГИКА СОБЫТИЙ "TEventSource" #####">
    protected class EventLogic extends ATOMNetworkProtocol.EventLogic        //Альтернатива 1. Для ведения собственного учета слушателей 
    // и логики реакции на собственные события
//protected class EventLogic extends ATOMEventDelegateAdapter  //Альтернатива 2. Используется, когда логика обработки собственных событий
    // остается в классе-генераторе экземпляра события,
    // но учет слушателей осуществляется в родительском управляющем классе.
    // Необходимо раскомментировать определение методов AddListener и RemoveListener
    {

        /**
         * Логика проверок возможности перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход объекта в новое состояние возможен , false -
         * иначе (нужно прерывать событие)
         */
        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
            // if (Events.Event1.class.isAssignableFrom(eventClass))
            // {
            //    Events.Event1  event = (Events.Event1)e;
            //    isAllowEvent = OnBefore_Event1(event);
            // }
            //2. Событие Events.Event2
            //... 
            return isAllowEvent ? super.OnBeforeEvent(eventSource, e) : false; // Вызов слушателей
        }

        /**
         * Логика перехода объекта в новое состояние
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         * @return true - переход в новое состояние успешно произведен, false -
         * иначе
         */
        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработчик события Events.Event1
            if (Events.LogInRequest.class.isAssignableFrom(eventClass))
            {
                Events.LogInRequest event = (Events.LogInRequest) e;
                isDone = OnDo_LogInRequest(event);
            }

            //1. Обработчик события Events.Event1
            if (Events.LogInResponce.class.isAssignableFrom(eventClass))
            {
                Events.LogInResponce event = (Events.LogInResponce) e;
                isDone = OnDo_LogInResponce(event);
            }

            if (Events.UserDisconnected.class.isAssignableFrom(eventClass))
            {
                Events.UserDisconnected event = (Events.UserDisconnected) e;
                isDone = OnDo_UserDisconnected(event);
            }

            if (Events.UserLoggedIn.class.isAssignableFrom(eventClass))
            {
                Events.UserLoggedIn event = (Events.UserLoggedIn) e;
                isDone = OnDo_UserLoggedIn(event);
            }
            
            if (Events.UserLoggedOff.class.isAssignableFrom(eventClass))
            {
                Events.UserLoggedOff event = (Events.UserLoggedOff) e;
                isDone = OnDo_UserLoggedOff(event);
            }

            return isDone ? super.OnDoEvent(eventSource, e) : false; // Вызов слушателей, если объект изменил состояние
        }

        /**
         * Логика синхронизации подчиненных элементов объекта с его новым
         * состоянием
         *
         * @param eventSource Объект, который генерирует уведомления
         * @param e Аргументы события
         */
        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            Class<?> eventClass = e.getClass();

            // //1. Обработчик события Events.Event1
             if (Events.UserLoggedIn.class.isAssignableFrom(eventClass))
             {
                Events.UserLoggedIn  event = (Events.UserLoggedIn)e;
                OnAfter_UserLoggedIn(event);
            
             }
            super.OnAfterEvent(eventSource, e);
        }

//  /**
//   * Добавление слушателя. Метод используется, для перенаправления учета слушателей в родительском управляющем классе
//   * (Ячейка таблицы -> Строка таблицы -> Таблица,  Атрибут сущности -> Сущность и т.п.)
//   * @param eventClass Тип экземпляра события
//   * @param listener Слушатель
//   * @param targetSource Источник событий, от которого должен получать уведомления слушатель
//   */
//  @Override
//  public void AddListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
//  {
//    //Логика перенаправления учета слушателей управляющему классу
//    _OwnerMangerEntity.Delegate().AddListener(eventClass, listener, targetSource);
//  }
        //  /**
        //  * Удаление слушателя.Метод используется, для перенаправления учета слушателей в родительском управляющем классе
        //  * @param eventClass Тип экземпляра события
        //  * @param listener Слушатель
        //  * @param targetSource Источник событий, от которого должен перстать получать уведомления слушатель
        //  * @return 
        //  */
        // @Override
        // public boolean RemoveListener(Class<? extends IATOMEvent> eventClass, IATOMEventListener listener, Object targetSource)
        // {
        //   //Логика перенаправления учета слушателей управляющему классу
        //   return _OwnerMangerEntity.Delegate().RemoveListener(eventClass, listener, targetSource);
        // }
    } //--- END CLASS EventLogic

//</editor-fold> ЛОГИКА СОБЫТИЙ "TEventSource" 
//##################################################################
}
