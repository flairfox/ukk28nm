/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.Protocols.Protocol_UsersRegistration;

import atom.errors.ATOMError;
import atom.names.ATOMStringResource;
import atom.names.IATOMName;
import atom.network.ATOMNetworkCommand;
import atom.network.ATOMNetworkProtocol;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;
import org.json.JSONObject;
import ukk28nm.Protocols.EUKK28NM_ProtocolSideRoles;

/**
 *
 * @author Добровольский
 */
public class Command_LogInRequest extends ATOMNetworkCommand
{

    public static IATOMName COMMAND_NAME = new ATOMStringResource("Command_LogInRequest", "Отправка регистрационной информации серверу");
    public static IATOMName USER_ID = new ATOMStringResource("UserID", "ID пользователя");
    public static IATOMName USER_ROLE = new ATOMStringResource("UserRole", "Роль пользователя");

    public UUID UserID;
    public EUKK28NM_ProtocolSideRoles UserRole;

    public Command_LogInRequest(UUID protocolID)
    {
        _CommonBlock.ProtocolID = protocolID;
    }

    @Override
    public IATOMName GetCommandName()
    {
        return COMMAND_NAME;
    }

    @Override
    public void Serialize(JSONObject rawNetworkPackage)
    {
        super.Serialize(rawNetworkPackage);
        
        try
        {
            // Сериализация полей команды и командых блоков (при наличии)
            rawNetworkPackage.put(USER_ID.LocalName(), UserID.toString());
            
            rawNetworkPackage.put(USER_ROLE.LocalName(), UserRole.getRoleName());

        } catch (JSONException ex)
        {
            Logger.getLogger(Command_LogInRequest.class.getName()).log(Level.SEVERE, null, ex);
        }

        super.Serialize(rawNetworkPackage); // записываем базовые данные команды
    }

    @Override
    public ATOMError Deserialize(JSONObject rawNetworkPackage)
    {
        super.Deserialize(rawNetworkPackage); // Расшифровываем базовые данные команды

        UserID = UUID.fromString(GetStringValue(USER_ID.LocalName()));
        String userRoleName = GetStringValue(USER_ROLE.LocalName());
        
        if (userRoleName.equals("Teacher")) {
            UserRole = EUKK28NM_ProtocolSideRoles.TEACHER;
        } else if (userRoleName.equals("Student")) {
            UserRole = EUKK28NM_ProtocolSideRoles.STUDENT;
        }

        return null;
    }
}
