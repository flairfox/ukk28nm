/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ukk28nm.ServerLogic;

import atom.events2.ATOMEventListenerAdapter;
import atom.events2.IATOMEvent;
import atom.network.ATOMListeningSocket_Abstract;
import atom.network.ATOMListeningSocket_Network;
import atom.network.ATOMNetworkCommand;
import atom.network.ATOMNetworkProtocol;
import atom.network.ATOMSocket_Abstract;
import java.util.HashMap;
import java.util.UUID;
import ukk28nm.Configurations.Environment_UKK28NM;
import ukk28nm.Protocols.EUKK28NM_ProtocolSideRoles;
import ukk28nm.Protocols.Protocol_Chat.Protocol_Chat;
import ukk28nm.Protocols.Protocol_EducationalMode_SectionsAdmittance.Protocol_EducationalMode_SectionsAdmittance;
import ukk28nm.Protocols.Protocol_EducationalMode_ViewAsTeacher.Protocol_EducationalMode_ViewAsTeacher;
import ukk28nm.Protocols.Protocol_GroupLessonOrganization.Protocol_GroupLessonOrganization;
import ukk28nm.Protocols.Protocol_StudentActivityMonitoring.Protocol_StudentActivityMonitoring;
import ukk28nm.Protocols.Protocol_UsersRegistration.Protocol_UserRegistration;

/**
 *
 * @author Vadim
 */
public class UKK28NM_Server
{

    protected ATOMListeningSocket_Abstract _ServerSocket;
    protected HashMap<UUID, ATOMSocket_Abstract> _UserSockets;

    protected Protocol_UserRegistration _UserRegistrationProtocol;
    protected Protocol_GroupLessonOrganization _LessonsProtocol;
    protected Protocol_StudentActivityMonitoring _StudentActivityMonitoring;
    protected Protocol_EducationalMode_ViewAsTeacher _ViewAsTeacher;
    protected Protocol_EducationalMode_SectionsAdmittance _SectionsAdmittance;
    protected Protocol_Chat _ChatProtocol;
    // some more protocols

    /**
     * Запустить логику серверной части УКК-28НМ
     * @return true - серверная логика запущена, false- иначе
     */
    public boolean start()
    {
        int listeningPortNumber = Environment_UKK28NM.Current().ServerPort;
        _ServerSocket = new ATOMListeningSocket_Network(listeningPortNumber);
        if (!_ServerSocket.IsListeningPort()) return false;

        _ServerSocket.Delegate().AddListener(_CommonListener);
        _ServerSocket.WaitIncomingConnectionAsync();

        //1. Инициализация протоколов
        //1.1 Протокол регистрации подключившихся пользователей (которые запустили программу)
        _UserRegistrationProtocol = new Protocol_UserRegistration(EUKK28NM_ProtocolSideRoles.SERVER, _ServerSocket);
        _UserRegistrationProtocol.Delegate().AddListener(_CommonListener);

        //1.2 Протокол организации групповых занятий
        _LessonsProtocol = new Protocol_GroupLessonOrganization(EUKK28NM_ProtocolSideRoles.SERVER, _UserRegistrationProtocol, _ServerSocket);
        _LessonsProtocol.Delegate().AddListener(_CommonListener);

        //1.3 Протокол мониторинга актиности студента
        _StudentActivityMonitoring = new Protocol_StudentActivityMonitoring(EUKK28NM_ProtocolSideRoles.SERVER, _UserRegistrationProtocol, _LessonsProtocol, _ServerSocket);
        _StudentActivityMonitoring.Delegate().AddListener(_CommonListener);

        //1.4 Протокол режима кадр как у преподавателя
        _ViewAsTeacher = new Protocol_EducationalMode_ViewAsTeacher(EUKK28NM_ProtocolSideRoles.SERVER, _UserRegistrationProtocol, _LessonsProtocol, _ServerSocket);
        _ViewAsTeacher.Delegate().AddListener(_CommonListener);

        //1.5 Протокол режима ограничения разделов
        _SectionsAdmittance = new Protocol_EducationalMode_SectionsAdmittance(EUKK28NM_ProtocolSideRoles.SERVER, _UserRegistrationProtocol, _LessonsProtocol, _ServerSocket);
        _SectionsAdmittance.Delegate().AddListener(_CommonListener);

        //1.6 Протокол чата
        _ChatProtocol = new Protocol_Chat(_ServerSocket);
        _ChatProtocol.Delegate().AddListener(_CommonListener);
        _ChatProtocol.Start();
        
        System.out.println("Server status: Online...");
        
        return true;
    }

    private boolean OnDo_NewConnectionSetup(ATOMListeningSocket_Abstract.Events.NewConnectionSetup event)
    {
        return true;
    }

    private boolean OnDo_RemoteSideCloseConnection(ATOMListeningSocket_Abstract.Events.RemoteSideCloseConnection event)
    {
        return true;
    }

    private boolean OnDoHandler_UserRegistered(Protocol_UserRegistration.Events.LogInRequest event)
    {
        return true;
    }

    private boolean OnDoHandler_NewConnectionSetup(ATOMNetworkProtocol.Events.NewConnectionSetup event)
    {
        if (event.Protocol instanceof Protocol_UserRegistration)
        {
            System.out.println(String.format("New unregistered user connected: connectionID = %s; Protocol = %s",
                    event.ConnectionSocket.getConnectionID().toString(),
                    event.Protocol.toString()));

        }

        return true;
    }

    protected boolean OnDoHandler_RemoteClientDisconnect(ATOMNetworkProtocol.Events.RemoteSideCloseConnection event)
    {
        if (event.Protocol instanceof Protocol_UserRegistration)
        {
            System.out.println(String.format("Client disconnected: connectionID = %s; Protocol = %s",
                    event.DisconnectedSocket.getConnectionID().toString(),
                    event.Protocol.toString()));
        }
        return true;
    }

    protected boolean OnDoHandler_IncomingNetworkCommandProcess(ATOMNetworkProtocol.Events.IncomingNetworkCommandProcess event) {
        ATOMNetworkCommand cmd = event.IncomingCommand;
        System.out.println(String.format("Клиент %s прислал команду %s",
                event.SocketToResponceSender.getConnectionID().toString(),
                cmd.toString()));
        return true;
    }

    

    //##################################################################
    //<editor-fold defaultstate="collapsed" desc="##### УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ #####">
    protected CommonListener _CommonListener = new CommonListener();

    protected class CommonListener extends ATOMEventListenerAdapter
    {

        @Override
        public boolean OnBeforeEvent(Object eventSource, IATOMEvent e)
        {
            boolean isAllowEvent = true;
            Class<?> eventClass = e.getClass();

            //1. Обработка события TEventSource.Events.Event1
            //if (TEventSource.Events.Event1.class.isAssignableFrom(eventClass))
            //{
            //  TEventSource.Events.Event1 event = (TEventSource.Events.Event1) e;
            //  isAllowEvent = OnBefore_Event1(event);
            //}
            return isAllowEvent;
        }

        @Override
        public boolean OnDoEvent(Object eventSource, IATOMEvent e)
        {
            boolean isDone = true;
            Class<?> eventClass = e.getClass();

            //1. Обработка события TEventSource.Events.Event1
            if (ATOMListeningSocket_Abstract.Events.NewConnectionSetup.class.isAssignableFrom(eventClass))
            {
                ATOMListeningSocket_Abstract.Events.NewConnectionSetup event = (ATOMListeningSocket_Abstract.Events.NewConnectionSetup) e;
                isDone = OnDo_NewConnectionSetup(event);
            }

            //2. Обработка события TEventSource.Events.Event2
            if (ATOMListeningSocket_Abstract.Events.RemoteSideCloseConnection.class.isAssignableFrom(eventClass))
            {
                ATOMListeningSocket_Abstract.Events.RemoteSideCloseConnection event = (ATOMListeningSocket_Abstract.Events.RemoteSideCloseConnection) e;
                isDone = OnDo_RemoteSideCloseConnection(event);
            }

            //3. Обработка события TEventSource.Events.Event3
            if (Protocol_UserRegistration.Events.LogInRequest.class.isAssignableFrom(eventClass))
            {
                Protocol_UserRegistration.Events.LogInRequest event = (Protocol_UserRegistration.Events.LogInRequest) e;
                isDone = OnDoHandler_UserRegistered(event);
            }
            
            if (Protocol_UserRegistration.Events.NewConnectionSetup.class.isAssignableFrom(eventClass))
            {
                Protocol_UserRegistration.Events.NewConnectionSetup event = (Protocol_UserRegistration.Events.NewConnectionSetup) e;
                isDone = OnDoHandler_NewConnectionSetup(event);
            }

            // Обработка команд чата
            if (ATOMNetworkProtocol.Events.IncomingNetworkCommandProcess.class.isAssignableFrom(eventClass)) 
            {
                ATOMNetworkProtocol.Events.IncomingNetworkCommandProcess event = (ATOMNetworkProtocol.Events.IncomingNetworkCommandProcess) e;
                isDone = OnDoHandler_IncomingNetworkCommandProcess(event);
            }

            // Обработка отсоединения клиента
            if (ATOMNetworkProtocol.Events.RemoteSideCloseConnection.class.isAssignableFrom(eventClass)) {
                ATOMNetworkProtocol.Events.RemoteSideCloseConnection event = (ATOMNetworkProtocol.Events.RemoteSideCloseConnection) e;
                isDone = OnDoHandler_RemoteClientDisconnect(event);
            }

            return isDone;
        }

        @Override
        public void OnAfterEvent(Object eventSource, IATOMEvent e)
        {
            //1. Обработка события TEventSource.Events.Event1
            Class<?> eventClass = e.getClass();

            //if (TEventSource.Events.Event1.class.isAssignableFrom(eventClass))
            //{
            //  TEventSource.Events.Event1 event = (TEventSource.Events.Event1) e;
            //  isDone  = OnAfter_Event1(event);
            // }
        }
    }
//</editor-fold> КОНЕЦ "УНИВЕРСАЛЬНЫЙ СЛУШАТЕЛЬ"
//##################################################################

}
